-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2016 at 12:16 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `infopark_siddhivinayak`
--

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name2` varchar(200) NOT NULL,
  `email2` varchar(200) NOT NULL,
  `message2` varchar(1000) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`contact_id`, `name2`, `email2`, `message2`) VALUES
(3, 'asdasd', 'asdas@g.com', 'asdasdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `c_id` int(255) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(50) NOT NULL,
  `c_gender` varchar(10) NOT NULL,
  `c_dob` varchar(20) NOT NULL,
  `c_contact` varchar(13) NOT NULL,
  `c_password` varchar(30) NOT NULL,
  `c_email` varchar(60) NOT NULL,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `c_address` varchar(300) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`c_id`, `c_name`, `c_gender`, `c_dob`, `c_contact`, `c_password`, `c_email`, `c_date`, `c_address`) VALUES
(1, 'Admin', 'Male', '1978-11-01', '8817919016', '8817919016', 'infopark_siddhivinayak@gmail.com', '2015-11-17 01:19:08', 'Bhopal');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `sid` int(255) NOT NULL AUTO_INCREMENT,
  `sname` varchar(30) NOT NULL,
  `smessage` varchar(500) NOT NULL,
  `simage` varchar(100) NOT NULL,
  `scity` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`sid`, `sname`, `smessage`, `simage`, `scity`, `date`, `time`) VALUES
(3, 'Lalit Pastor', 'Vision without action is merely a dream. Action without vision merely passes the time. Vision with action can change the worldâ€.But in my case, I had a vision to work in a field where I can serve the customers with my hospitality. At that time of despair, Highline Educare helped me by guiding how I can chase my goal. They enrolled me in one of the hospitality program of M.O.R.D. After 2 months of training program, I immediately got a Job in Amer Palace with a starting salary of Rs. 8,000/- per', 'upload/1455480599.png', 'Bhopal', 'Mon, 15 Feb 2016', '01:39:59 am'),
(4, 'Ankit Tiwari', 'My name is Ankit Tiwari. Its been my pleasure to attain a training program in Retail Sector at Highline Educare. The course module and the practical classes where very systematic which helped in developing a professional attitude which is unforgettable. After the completion of the training program, I have been recruited in Best Prize as a Customer Relation Executive with a salary of Rs. 15,000/- per month. I want to thank Highline Educare for all the sincere efforts taken by them in order t', 'upload/1455550752.png', 'Vidisha', 'Mon, 15 Feb 2016', '01:40:48 am');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `iid` int(255) NOT NULL AUTO_INCREMENT,
  `ipath` varchar(200) NOT NULL,
  `icat` varchar(30) NOT NULL,
  `idate` varchar(50) NOT NULL,
  `itime` varchar(50) NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`iid`, `ipath`, `icat`, `idate`, `itime`) VALUES
(11, 'upload/1472725712.png', 'School', 'Thu, 01 Sep 2016', '03:58:32 pm'),
(12, 'upload/1472725719.png', 'School', 'Thu, 01 Sep 2016', '03:58:39 pm'),
(13, 'upload/1472725726.png', 'School', 'Thu, 01 Sep 2016', '03:58:46 pm'),
(14, 'upload/1472725735.png', 'School', 'Thu, 01 Sep 2016', '03:58:55 pm'),
(15, 'upload/1472725742.png', 'School', 'Thu, 01 Sep 2016', '03:59:02 pm'),
(16, 'upload/1472725748.png', 'School', 'Thu, 01 Sep 2016', '03:59:08 pm'),
(17, 'upload/1472725753.png', 'School', 'Thu, 01 Sep 2016', '03:59:13 pm'),
(18, 'upload/1472725760.png', 'School', 'Thu, 01 Sep 2016', '03:59:20 pm'),
(19, 'upload/1472725764.png', 'School', 'Thu, 01 Sep 2016', '03:59:24 pm'),
(20, 'upload/1472725772.png', 'School', 'Thu, 01 Sep 2016', '03:59:32 pm'),
(21, 'upload/1472725781.png', 'School', 'Thu, 01 Sep 2016', '03:59:41 pm'),
(22, 'upload/1472725785.png', 'School', 'Thu, 01 Sep 2016', '03:59:45 pm'),
(23, 'upload/1473942381.png', '', 'Thu, 15 Sep 2016', '05:56:21 pm');

-- --------------------------------------------------------

--
-- Table structure for table `imagecategory`
--

CREATE TABLE IF NOT EXISTS `imagecategory` (
  `c_id` int(255) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `imagecategory`
--

INSERT INTO `imagecategory` (`c_id`, `cname`) VALUES
(5, 'School '),
(6, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `nid` int(255) NOT NULL AUTO_INCREMENT,
  `nheading` varchar(255) NOT NULL,
  `ncontent` varchar(20000) NOT NULL,
  `ndate` varchar(30) NOT NULL,
  `nimage` varchar(100) NOT NULL,
  `nposted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`nid`, `nheading`, `ncontent`, `ndate`, `nimage`, `nposted`) VALUES
(1, 'CERTIFICATE AND REWARD DISTRIBUTION TO SUCCESSFUL 500 PMKVY CANDIDATES', 'Recently, HEI conducted a program of CERTIFICATE AND REWARD MONEY DISTRIBUTION TO 500 PMKVY CANDIDATES who successfully cleared their assessments after 30 days training program.', '2016-02-03', 'upload/1455487168.png', '2016-02-14 21:59:28'),
(5, 'Parties stand divided over sedition law', 'With Jawaharlal Nehru University Studentsâ€™ Union president Kanhaiya Kumar being charged with sedition, opinion across the political spectrum stands divided over what constitutes â€˜sedition.â€™', '2016-02-16', 'upload/1455610110.png', '2016-02-16 08:08:31'),
(7, 'welcome to the new wbsite of svps bhopal', 'new website of svps bhopal is lanched', '2016-09-01', 'upload/1472796710.png', '2016-09-02 06:11:50');

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE IF NOT EXISTS `queries` (
  `qid` int(255) NOT NULL AUTO_INCREMENT,
  `qname` varchar(40) NOT NULL,
  `qemail` varchar(60) NOT NULL,
  `qcontact` varchar(10) NOT NULL,
  `qsubject` varchar(40) NOT NULL,
  `qmessage` varchar(500) NOT NULL,
  `qdate` varchar(30) NOT NULL,
  `qtime` varchar(30) NOT NULL,
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`qid`, `qname`, `qemail`, `qcontact`, `qsubject`, `qmessage`, `qdate`, `qtime`) VALUES
(3, 'fdf', 'dsf@gmail.com', '7777777777', 'sd bfb', 'sndsfn', 'Wed, 13 Jan 2016', '02:49:31 am'),
(4, 'ghhh', 'fh@gmail.com', '4545454545', 'gfhh', 'gfhhf', 'Wed, 13 Jan 2016', '03:18:50 pm'),
(5, 'vb', 'cvb@g.com', '4454444444', 'bhcfbbbv', 'vcbbvbc', 'Wed, 13 Jan 2016', '03:21:46 pm'),
(6, 'vc', 'a@gmail.com', '4444444444', 'jghjgjjj', 'gjgfhjhj', 'Wed, 13 Jan 2016', '03:25:46 pm'),
(7, 'dfdfg', 'dfg@gmail.com', '4444444444', 'bjkbkj', 'kbhkk', 'Wed, 13 Jan 2016', '03:33:50 pm'),
(8, 'cbvb', 'vcbvb@gmail.com', '4444444444', 'fgcfgfh', '4545', 'Wed, 13 Jan 2016', '03:35:23 pm'),
(9, 'hjg', 'gj@gmail.com', '4444444444', 'fgjj', 'ghfgh', 'Wed, 13 Jan 2016', '03:38:31 pm'),
(10, 'vhvg', 'g@gmail.com', '4633333333', 'dsgfggg', 'fgfdgdgg', 'Wed, 13 Jan 2016', '03:42:11 pm'),
(11, 'xgfhh', 'h@gmail.com', '5644444444', 'fgdfghghf', 'fghdfhghfg', 'Wed, 13 Jan 2016', '03:42:35 pm'),
(12, 'gdffg', 'fgf@gmail.com', '4444444444', 'hgjhjh', 'fgdfgf', 'Wed, 13 Jan 2016', '04:12:17 pm'),
(13, 'test', 't@gmail.com', '7773070823', 'we', 'fgghdg', 'Wed, 31 Aug 2016', '11:37:45 am'),
(14, 'jj', 'jj@gmail.com', '7773070823', 'ggg', 'gfgfgdgdg', 'Wed, 14 Sep 2016', '02:38:04 pm');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sch_number` varchar(255) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `fcontact` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `pic_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `sch_number`, `sname`, `fname`, `fcontact`, `class`, `date`, `pic_path`) VALUES
(5, '0133IT081023', 'Lalit Pastor', 'Satyanarayan Pastor', '7773070823', 'XII', '23/02/1989', 'upload_student/1474020370.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_uploads`
--

CREATE TABLE IF NOT EXISTS `tbl_uploads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_uploads`
--

INSERT INTO `tbl_uploads` (`id`, `file`, `type`, `size`) VALUES
(1, '26490-prakash.pdf', 'application/pdf', 83),
(2, '71135-prakash.docx', 'application/vnd.openxmlformats', 10),
(3, '87149-schools.xlsx', 'application/vnd.openxmlformats', 9);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
