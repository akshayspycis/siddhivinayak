<?php

    class faqCategory {
        private $fc_id;
        private $fc_name;
        
        function getFc_id() {
            return $this->fc_id;
        }

        function getFc_name() {
            return $this->fc_name;
        }

        function setFc_id($fc_id) {
            $this->fc_id = $fc_id;
        }
        function setFc_name($fc_name) {
            $this->fc_name = $fc_name;
        }
}
?>
