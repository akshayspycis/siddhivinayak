<?php 
    class Customers{
        private $c_id;
        private $c_name;
        private $c_gender;
        private $c_dob;
        private $c_contact;
        private $c_password;
        private $c_email;
        private $c_date;
        private $c_address;
        
        public function getC_id() {
            return $this->c_id;
        }

        public function setC_id($c_id) {
            $this->c_id = $c_id;
        }

        public function getC_name() {
            return $this->c_name;
        }

        public function setC_name($c_name) {
            $this->c_name = $c_name;
        }

        public function getC_gender() {
            return $this->c_gender;
        }

        public function setC_gender($c_gender) {
            $this->c_gender = $c_gender;
        }

        public function getC_dob() {
            return $this->c_dob;
        }

        public function setC_dob($c_dob) {
            $this->c_dob = $c_dob;
        }

        public function getC_contact() {
            return $this->c_contact;
        }

        public function setC_contact($c_contact) {
            $this->c_contact = $c_contact;
        }

        public function getC_password() {
            return $this->c_password;
        }

        public function setC_password($c_password) {
            $this->c_password = $c_password;
        }

        public function getC_email() {
            return $this->c_email;
        }

        public function setC_email($c_email) {
            $this->c_email = $c_email;
        }

        public function getC_date() {
            return $this->c_date;
        }

        public function setC_date($c_date) {
            $this->c_date = $c_date;
        }

        public function getC_address() {
            return $this->c_address;
        }

        public function setC_address($c_address) {
            $this->c_address = $c_address;
        }

}
?>