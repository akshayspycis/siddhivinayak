<?php

    class faq{
        private $f_id;
        private $f_question;
        private $f_answer;
        private $fc_id;
        private $f_date;
        
        function getF_id() {
            return $this->f_id;
        }

        function getF_question() {
            return $this->f_question;
        }

        function getF_answer() {
            return $this->f_answer;
        }

        function getFc_id() {
            return $this->fc_id;
        }

        function getF_date() {
            return $this->f_date;
        }

        function setF_id($f_id) {
            $this->f_id = $f_id;
        }

        function setF_question($f_question) {
            $this->f_question = $f_question;
        }

        function setF_answer($f_answer) {
            $this->f_answer = $f_answer;
        }

        function setFc_id($fc_id) {
            $this->fc_id = $fc_id;
        }

        function setF_date($f_date) {
            $this->f_date = $f_date;
        }

 
}
?>
