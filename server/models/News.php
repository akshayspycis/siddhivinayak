<?php
    class News {

        private $nid;
        private $nheading;
        private $ncontent;
        private $ndate;
        private $nimage;
        private $nposted;
        
        function getNid() {
            return $this->nid;
        }

        function getNheading() {
            return $this->nheading;
        }

        function getNcontent() {
            return $this->ncontent;
        }

        function getNdate() {
            return $this->ndate;
        }

        function getNimage() {
            return $this->nimage;
        }

        function getNposted() {
            return $this->nposted;
        }

        function setNid($nid) {
            $this->nid = $nid;
        }

        function setNheading($nheading) {
            $this->nheading = $nheading;
        }

        function setNcontent($ncontent) {
            $this->ncontent = $ncontent;
        }

        function setNdate($ndate) {
            $this->ndate = $ndate;
        }

        function setNimage($nimage) {
            $this->nimage = $nimage;
        }

        function setNposted($nposted) {
            $this->nposted = $nposted;
        }


      

    }


