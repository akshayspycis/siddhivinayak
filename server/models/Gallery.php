<?php 

class Gallery {

        private $iid;
        private $ipath;
        private $icat;
        private $idate;
        private $itime;
        
        function getIid() {
            return $this->iid;
        }

        function getIpath() {
            return $this->ipath;
        }

        function getIcat() {
            return $this->icat;
        }

        function getIdate() {
            return $this->idate;
        }

        function getItime() {
            return $this->itime;
        }

        function setIid($iid) {
            $this->iid = $iid;
        }

        function setIpath($ipath) {
            $this->ipath = $ipath;
        }

        function setIcat($icat) {
            $this->icat = $icat;
        }

        function setIdate($idate) {
            $this->idate = $idate;
        }

        function setItime($itime) {
            $this->itime = $itime;
        }

}



?>
