<?php

    class imgCategory {
        
        private $c_id;
        private $cname;
        
        public function getC_id() {
            return $this->c_id;
        }

        public function setC_id($c_id) {
            $this->c_id = $c_id;
        }

        public function getCname() {
            return $this->cname;
        }

        public function setCname($cname) {
            $this->cname = $cname;
        }
}

?>
