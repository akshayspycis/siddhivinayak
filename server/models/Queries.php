<?php
    class Queries {

        private $qid;
        private $qname;
        private $qemail;
        private $qcontact;
        private $qsubject;
        private $qmessage;
        private $qdate;
        private $qtime;
        
        function getQid() {
            return $this->qid;
        }

        function getQname() {
            return $this->qname;
        }

        function getQemail() {
            return $this->qemail;
        }

        function getQcontact() {
            return $this->qcontact;
        }

        function getQsubject() {
            return $this->qsubject;
        }

        function getQmessage() {
            return $this->qmessage;
        }

        function getQdate() {
            return $this->qdate;
        }

        function getQtime() {
            return $this->qtime;
        }

        function setQid($qid) {
            $this->qid = $qid;
        }

        function setQname($qname) {
            $this->qname = $qname;
        }

        function setQemail($qemail) {
            $this->qemail = $qemail;
        }

        function setQcontact($qcontact) {
            $this->qcontact = $qcontact;
        }

        function setQsubject($qsubject) {
            $this->qsubject = $qsubject;
        }

        function setQmessage($qmessage) {
            $this->qmessage = $qmessage;
        }

        function setQdate($qdate) {
            $this->qdate = $qdate;
        }

        function setQtime($qtime) {
            $this->qtime = $qtime;
        }






    }


