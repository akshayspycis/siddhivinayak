<?php
    class Feedback {

        private $sid;
        private $sname;
        private $smessage;
        private $simage;
        private $scity;
        private $date;
        private $time;
        
        function getSid() {
            return $this->sid;
        }

        function getSname() {
            return $this->sname;
        }

        function getSmessage() {
            return $this->smessage;
        }

        function getSimage() {
            return $this->simage;
        }

        function getScity() {
            return $this->scity;
        }

        function getDate() {
            return $this->date;
        }

        function getTime() {
            return $this->time;
        }

        function setSid($sid) {
            $this->sid = $sid;
        }

        function setSname($sname) {
            $this->sname = $sname;
        }

        function setSmessage($smessage) {
            $this->smessage = $smessage;
        }

        function setSimage($simage) {
            $this->simage = $simage;
        }

        function setScity($scity) {
            $this->scity = $scity;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setTime($time) {
            $this->time = $time;
        }


    }


