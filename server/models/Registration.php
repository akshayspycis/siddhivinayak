<?php
    class Registration {

        private $id;
        private $sch_number;
        private $sname;
        private $fname;
        private $fcontact;
        private $class;
        private $date;
        private $pic_path;
        private $password;
        
        public function getId() {
            return $this->id;
        }

        public function getSch_number() {
            return $this->sch_number;
        }

        public function getSname() {
            return $this->sname;
        }

        public function getFname() {
            return $this->fname;
        }

        public function getFcontact() {
            return $this->fcontact;
        }

        public function getClass() {
            return $this->class;
        }

        public function getDate() {
            return $this->date;
        }

        public function getPic_path() {
            return $this->pic_path;
        }

        public function getPassword() {
            return $this->password;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function setSch_number($sch_number) {
            $this->sch_number = $sch_number;
        }

        public function setSname($sname) {
            $this->sname = $sname;
        }

        public function setFname($fname) {
            $this->fname = $fname;
        }

        public function setFcontact($fcontact) {
            $this->fcontact = $fcontact;
        }

        public function setClass($class) {
            $this->class = $class;
        }

        public function setDate($date) {
            $this->date = $date;
        }

        public function setPic_path($pic_path) {
            $this->pic_path = $pic_path;
        }

        public function setPassword($password) {
            $this->password = $password;
        }


}



