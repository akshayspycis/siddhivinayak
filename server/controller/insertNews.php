<?php
    include_once '../models/News.php'; 
    include_once '../managers/NewsMgr.php'; 
    
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    
    $news = new News();
    $news->setNheading($_POST["nheading"]);
    $news->setNdate($_POST["ndate"]);  
    $news->setNcontent($_POST["ncontent"]);  
    
    $upload_dir = "upload/";
    $img = $_POST["nimage"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $news->setNimage($file);   
      
    $newsMgr = new NewsMgr();
    if ($newsMgr->insNews($news)) {
        echo 'News inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>