<?php
     include_once '../models/ContactUs.php';
    include_once '../managers/ContactUsMgr.php';
    $obj = new ContactUsMgr();
    $customers = $obj->selEnquiry();
    $str = array();    
    while($row = $customers->fetch()) {
        $arr = array(
            'contact_id' => $row['contact_id'], 
            'name2' => $row['name2'],             
            'email2' => $row['email2'],             
            'message2' => $row['message2']            
        );
        array_push($str, $arr); 
        
    }
    echo json_encode($str);
?>
