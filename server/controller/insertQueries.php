<?php
    include_once '../models/Queries.php'; 
    include_once '../managers/QueriesMgr.php'; 
    
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $queries = new Queries();
    $queries->setQname($_POST["qname"]);
    $queries->setQemail($_POST["qemail"]);  
    $queries->setQcontact($_POST["qcontact"]);   
    $queries->setQsubject($_POST["qsubject"]);   
    $queries->setQmessage($_POST["qmessage"]);   
    $queries->setQdate($date->format('D, d M Y'));   
    $queries->setQtime($date->format('h:i:s a'));   
    $queriesMgr = new QueriesMgr();
    if ($queriesMgr->insQueries($queries)) {
        echo 'Query inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>