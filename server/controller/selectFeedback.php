<?php
     include_once '../models/Feedback.php';
    include_once '../managers/FeedbackMgr.php';
    $obj = new FeedbackMgr();
   
    $feedback = $obj->selFeedback();
    $str = array();    
    while($row = $feedback->fetch()){
        $arr = array(
            'sid' => $row['sid'], 
            'sname' => $row['sname'],             
            'smessage' => $row['smessage'],             
            'simage' => $row['simage'],             
            'scity' => $row['scity'],             
            'date' => $row['date'],             
            'time' => $row['time'],             
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>