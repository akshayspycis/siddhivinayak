<?php
     include_once '../models/News.php';
    include_once '../managers/NewsMgr.php';
    $obj = new NewsMgr();
   
    $news = $obj->selNews();
    $str = array();    
    while($row = $news->fetch()){
        $arr = array(
            'nid' => $row['nid'], 
            'nheading' => $row['nheading'],             
            'ncontent' => $row['ncontent'],             
            $ndate = date_create($row['ndate']),
            'ndate' => date_format($ndate, 'd/m/Y'),
            'nimage' => $row['nimage'], 
            $pdate = date_create($row['nposted']),
            'nposted' => date_format($pdate, 'd/m/Y g:i A'),           
        );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>
