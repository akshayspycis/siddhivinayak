<?php
     include_once '../models/Gallery.php';
    include_once '../managers/GalleryMgr.php';
    $obj = new GalleryMgr();
   
    $gallery = $obj->selImage();
    $str = array();    
    while($row = $gallery->fetch()){
        $arr = array(
            'iid' => $row['iid'], 
            'ipath' => $row['ipath'],
            'icat' => $row['icat'],
            'idate' => $row['idate'],             
            'itime' => $row['itime'],             
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>