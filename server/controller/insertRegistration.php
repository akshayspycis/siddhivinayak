<?php
    include_once '../models/Registration.php'; 
    include_once '../managers/RegistrationMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $registration = new Registration();
    $registration->setSch_number($_POST["sch_number"]);
    $registration->setSname($_POST["sname"]);
    $registration->setFname($_POST["fname"]);  
    $registration->setFcontact($_POST["fcontact"]);   
    $registration->setClass($_POST["class"]);   
    $registration->setDate($_POST["date"]);   
    $registration->setPic_path($_POST["pic_path"]);   
    $registration->setPassword($_POST["password"]);   
     
  
    $registrationMgr = new RegistrationMgr();
    if ($registrationMgr->insRegistration($registration)) {
        echo 'Query inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>