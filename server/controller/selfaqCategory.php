<?php
    include_once '../managers/faqcategoryMgr.php';
    
    $obj = new faqCategoryMgr();
    $category = $obj->selfaqCategory();
    
    $str = array();    
    while($row = $category->fetch()) {
        $arr = array(
            'fc_id' => $row['fc_id'], 
            'fc_name' => $row['fc_name'],             
            
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>