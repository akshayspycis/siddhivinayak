<?php
include_once '../models/customers.php';
include_once '../managers/CustomersMgr.php';
session_start();

    if(isset($_SESSION["c_id"])){
       
       $cd = new Customers(); 
       $cd->setC_id($_SESSION["c_id"]);
        $customerMgr = new CustomerMgr(); 
        $customers = $customerMgr->singleCustomer($cd);
        $str = array();    
        while($row = $customers->fetch()) {
        $arr = array(
            'c_id' => $row['c_id'], 
            'c_name' => $row['c_name'],             
            'c_gender' => $row['c_gender'],             
            'c_dob' => $row['c_dob'],             
            'c_contact' => $row['c_contact'],             
            'c_password' => $row['c_password'],             
            'c_email' => $row['c_email'],             
            'c_date' => $row['c_date'],             
            'c_address' => $row['c_address'],             
        );
        array_push($str, $arr); 
        
    } 
      echo json_encode($str);   
    }
    
?>