<?php
    include_once '../managers/imgcategoryMgr.php';
    
    $obj = new imgCategoryMgr();
    $category = $obj->selimgCategory();
    
    $str = array();    
    while($row = $category->fetch()) {
        $arr = array(
            'c_id' => $row['c_id'], 
            'name' => $row['cname'],             
            
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>