 <?php
    include_once '../models/News.php';
    include_once '../managers/NewsMgr.php';
    $news = new News();    
    $news->setNid($_POST["nid"]);
    $news->setNheading($_POST["nheading"]);
    $news->setNcontent($_POST["ncontent"]);
    $news->setNdate($_POST["ndate"]);
    
    $newsMgr = new NewsMgr();    
    
    if ($newsMgr->updateNews($news)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>