 <?php
    include_once '../models/faq.php';
    include_once '../managers/faqMgr.php';
    $faq = new faq();    
    $faq->setF_id($_POST["f_id"]);
    $faq->setF_question($_POST["f_question"]);
    $faq->setF_answer($_POST["f_answer"]);
    $faq->setFc_id($_POST["fc_id"]);
    $faqMgr = new faqMgr();    
    
    if ($faqMgr->updateFaq($faq)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>
