<?php
        include_once '../models/Queries.php';
        include_once '../managers/QueriesMgr.php';
    $obj = new QueriesMgr();
   
    $queries = $obj->selQueries();
    $str = array();    
    while($row = $queries->fetch()){
        $arr = array(
                'qid' => $row['qid'], 
                'qname' => $row['qname'],             
                'qemail' => $row['qemail'],             
                'qsubject' => $row['qsubject'],             
                'qmessage' => $row['qmessage'],             
                'qdate' => $row['qdate'],             
                'qtime' => $row['qtime'],             
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>