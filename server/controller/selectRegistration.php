<?php
        include_once '../models/Registration.php';
        include_once '../managers/RegistrationMgr.php';
    $obj = new RegistrationMgr();
   
    $registration = $obj->selRegistration();
    $str = array();    
    while($row = $registration->fetch()){
        $arr = array(
                'id' => $row['id'], 
                'sch_number' => $row['sch_number'], 
                'sname' => $row['sname'],             
                'fname' => $row['fname'],             
                'fcontact' => $row['fcontact'],             
                'class' => $row['class'],             
                'date' => $row['date'],             
                'pic_path' => $row['pic_path'],             
                'password' => $row['password'],             
                
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>