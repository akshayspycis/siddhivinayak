<?php
    include_once '../models/Gallery.php'; 
    include_once '../managers/GalleryMgr.php'; 
    
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    
    $gallery = new Gallery();
    $upload_dir = "upload/";
    $img = $_POST["ipath"];
    
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $gallery->setIpath($file);   
    $gallery->setIcat($_POST["icat"]);   
    $gallery->setIdate($date->format('D, d M Y'));   
    $gallery->setItime($date->format('h:i:s a'));   
    $galleryMgr = new GalleryMgr();
    if ($galleryMgr->insImage($gallery)) {
        echo 'Image inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>