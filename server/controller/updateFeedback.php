 <?php
    include_once '../models/Feedback.php';
    include_once '../managers/FeedbackMgr.php';
    $feedback = new Feedback();    
    $feedback->setSid($_POST["sid"]);
    $feedback->setSname($_POST["sname"]);
    $feedback->setSmessage($_POST["smessage"]);
    $feedback->setScity($_POST["scity"]);
    
    $feedbackMgr = new FeedbackMgr();    
    
    if ($feedbackMgr->updateFeedback($feedback)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>