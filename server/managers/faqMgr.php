<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class faqMgr{
        
//        method to insert enquiry in database
        public function insFaq(faq $faq) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO faq(f_question,f_answer,fc_id) VALUES ('".$faq->getF_question()."','".$faq->getF_answer()."','".$faq->getFc_id()."')";            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {   
                return TRUE;
            } else {
                return FALSE;
            }
        }
    
//        method to update faq in database
        public function updateFaq(faq $faq){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE faq SET " 
                    ."f_question='".$faq->getF_question()."',"
                    ."f_answer='".$faq->getF_answer()."'"
                    ."WHERE f_id=".$faq->getF_id()."";
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
//        method to delete faqcat in database
        public function delfaqCategory($fc_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from cfaq where fc_id = ".$fc_id."";
           
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        } 
       //method to delete faq in database
        public function delFaq($f_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from faq where f_id = '".$f_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to select enquiry from database
        public function selfaq() {
            $dbh = new DatabaseHelper();
            
            $sql = "SELECT faq.f_id,faq.f_question,faq.f_answer,cfaq.fc_name,faq.f_date FROM faq INNER JOIN cfaq ON faq.fc_id= cfaq.fc_id";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select enquiry data for update from database
//        public function singleCategory($c_id) {
//            $dbh = new DatabaseHelper();
//            $sql = "select * from faq where c_id = '".$c_id."'";
//            $stmt = $dbh->createConnection()->prepare($sql);            
//            $stmt->execute();
//            
//            $dbh->closeConnection();
//            return $stmt;
//        }

    }
?>
