<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class FeedbackMgr {    

        //method to insert feedback in database
        public function insFeedback(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO feedback(sname, smessage, simage, scity, date, time) VALUES ('".$feedback->getSname()."','".$feedback->getSmessage()."','".$feedback->getSimage()."','".$feedback->getScity()."','".$feedback->getDate()."','".$feedback->getTime()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delFeedback($sid) {
            $dbh = new DatabaseHelper();
            $sql = "delete from feedback where sid = '".$sid."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Feedback from database
        public function selFeedback() {
            $dbh = new DatabaseHelper();
            $sql = "select * from feedback";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateFeedback(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE feedback SET " 
                    ."sid='".$feedback->getSid()."',"."sname='".$feedback->getSname()."',"
                    ."smessage='".$feedback->getSmessage()."',"
                    ."scity='".$feedback->getScity()."'"
                    ."WHERE sid=".$feedback->getSid()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updFeedbackImg(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT feedback.simage FROM feedback WHERE sid=".$feedback->getSid()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['simage'];
            }
            if (unlink($file)){
                $sql ="UPDATE feedback SET " 
                ."simage='".$feedback->getSimage()."'"
                ."WHERE sid=".$feedback->getSid()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>
