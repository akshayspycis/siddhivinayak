<?php
    include_once '../dbhelper/DatabaseHelper.php';
    include_once '../models//ContactUs.php';
    
    class ContactUsMgr{
        
//        method to insert contactus in database
        public function insEnquiry(ContactUs $contactus) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO contactus(name2, email2, message2) VALUES ('".$contactus->getName2()."','".$contactus->getEmail2()."','".$contactus->getMessage2()."')";            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            
            if ($i > 0) {   
                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    
//        method to update contactus in database
        public function updateEnquiry(Enquiry $contactus) {
            $dbh = new DatabaseHelper();
                    
            $sql = $sql="UPDATE contactus SET " 
                       
                    . "name='".$contactus->getName()."',"
                    . "email='".$contactus->getEmail()."',"
                    . "subject='".$contactus->getSubject()."',"
                    . "message='".$contactus->getMessage()."'"
                   . " WHERE contactus_id='".$contactus->getEnquiry_id()."'";
             echo $sql;
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
//        method to delete contactus in database
        public function delEnquiry($contactus_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from contactus where contactus_id = '".$contactus_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        } 
        
        //method to select contactus from database
        public function selEnquiry() {
            $dbh = new DatabaseHelper();
            $sql = "select * from contactus";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select contactus data for update from database
        public function singleEnquiry($contactus_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from contactus where contactus_id = '".$contactus_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function countEnquiry() {
            $dbh = new DatabaseHelper();
            $sql = "select count(*) as count_data from contactus";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $stmt->execute();
            $dbh->closeConnection();
          return $stmt;
        }

    }
?>
