<?php
    include_once '../dbhelper/DatabaseHelper.php';
    include_once '../models/customers.php';
    
    
    class CustomerMgr{
        
//        method to insert enquiry in database
        public function insCustomer(Customers $customer) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO customers(c_name,c_gender,c_dob,c_contact,c_password,c_email,c_address)
                VALUES ('".$customer->getC_name()."',
                        '".$customer->getC_gender()."',
                        '".$customer->getC_dob()."',
                        '".$customer->getC_contact()."',
                        '".$customer->getC_password()."',
                        '".$customer->getC_email()."',
                        '".$customer->getC_address()."'
               )";            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {   
                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    
//        method to update enquiry in database
        public function updateCustomer(Customers $customer) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE customers SET " 
                    ."c_name='".$customer->getC_name()."',"
                    ."c_gender='".$customer->getC_gender()."',"
                    ."c_dob='".$customer->getC_dob()."',"
                    ."c_contact='".$customer->getC_contact()."',"
                    ."c_password='".$customer->getC_password()."',"
                    ."c_email='".$customer->getC_email()."',"
                    ."c_address='".$customer->getC_address()."'"
                    ."WHERE c_id=".$customer->getC_id()."";
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
//        method to delete enquiry in database
        public function delCustomer($c_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from customers where c_id = ".$c_id."";
           
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        } 
        
        //method to select enquiry from database
        public function selCustomer() {
            $dbh = new DatabaseHelper();
            $sql = "SELECT * FROM customers";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select customer data for update from database
        public function singleCustomer(Customers $cd) {
            $dbh = new DatabaseHelper();
            $sql = "select * from customers where c_id = '".$cd->getC_id()."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
             $stmt->execute();
            
            $dbh->closeConnection();
             return $stmt;
        }
        
        public function loginCustomer(Customers $customer) {
            $dbh = new DatabaseHelper();
            $sql = "select * from customers where c_contact = '".$customer->getC_contact()."' and c_password = '".$customer->getC_password()."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function countCustomer() {
            $dbh = new DatabaseHelper();
            $sql = "select count(*) as count_data from customers";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $stmt->execute();
            $dbh->closeConnection();
          return $stmt;
        }
       
    }
?>
