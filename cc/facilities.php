<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Facilities</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg clearfix" id="aboutr1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Facilities</h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara"><b>Library & Reading Room :</b><br/>We have a well equipped library and reading room stocked with a wide range of books covering different disciplines.The library acts as a centre of academic and intellectual development for staff and students, and contains encyclopedias, various series of world books and over 10,000 publications.The library subscribes to national and international periodicals, journals and newspapers.</p> 
                                        <p class="text-center" id="hpara"><b>Laboratories :</b><br/>Three science laboratories provide requisite hands-on experience which is the heart of science teaching. Science teachers along with the lab assistant plan various demonstrations and experiments to ignite the curiosity and inculcate a scientific temperament in the students.</p> 
                                        <p class="text-center" id="hpara"><b>Computer Lab :</b><br/>Two internet-enabled computer laboratories with over a 100 computers are designed to meet the students’ Information Technology needs. Trained and experienced teachers provide both theoretical and practical lessons for students to help them navigate a rapidly changing technology driven world.</p> 
                                        <p class="text-center" id="hpara"><b>Performing Arts :</b><br/>A dedicated room is used to hone skills in vocal and instrumental music, dance and drama. </p> 
                                        <p class="text-center" id="hpara"><b>Art and craft :</b><br/>A bright, airy and inspiring room, which  allows students to explore a variety of media ranging from fine art, painting, drawing and craft to give expression to their creativity.</p> 
                                        <p class="text-center" id="hpara"><b>Play area :</b><br/>The sand pit, slides and swings make coming to school an enjoyable experience for young children.</p> 
                                        <p class="text-center" id="hpara"><b>Sports Facilities :</b><br/>The School has good sports facilities, which include indoor and outdoor basketball courts, a squash court, table tennis facilities and a multipurpose ground. Opportunities for team games and physical training are facilitated by trained physical instructors and yoga teachers. Students practice yoga and pursue sports like table tennis, volleyball, basketball, badminton, throwball, squash and athletics. They also enjoy board games like carrom and chess. The annual sports meet is held to celebrate the spirit of sportsmanship.</p> 
                                       
                                       
                                       <br>
					
				</div>
			</section>
			<!-- section end -->
                    
                        
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                        <section class="section clearfix" id="hrow4">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3>Our <span class="text-default"> Vision</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
							<p id="hpara">We are committed to provide educational excellence for all.</p>
								
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Our <span class="text-default"> Mission</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
							<p id="hpara">We provide the highest quality education so that all of our students are empowered to lead 
productive and fulfilling lives as lifelong learners and responsible citizens.
</p>
								
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Our <span class="text-default">Values</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
                                                    <p id="hpara">We, the faculty and staff of the SVPS of Education, commit to the following values:  </p>
                                                    <p id="hpara"><b>:: Leadership</b><br> <b>:: Excellence</b><br>
                                                    <b>:: Integrity</b><br> <b>:: Citizenship</b></p>
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Campus <span class="text-default">Gallery</span></h3>
							<div class="separator-2"></div>
							<div class="owl-carousel content-slider-with-large-controls-autoplay dark-controls light-gray-bg buttons-hide" id="hbox">							
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus0.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left"></p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus1.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Practical Lab</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus2.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Library</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus3.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Play Ground</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
                        <?php include './includes/querybanner.php';?>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
</html>



