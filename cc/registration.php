<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Registration</title>
		<!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
                <style>
                .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
                h1, h5{ padding:0px; margin:0px; }
                h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
                .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
                .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
                </style>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                            	<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-8 space-bottom">
                                                    <p id="hpara"><b>It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</b></p>
                                                    <p>&nbsp;</p>
							<div class="alert alert-success hidden" id="MessageSent">
								We have received your message, we will contact you very soon.
							</div>
							<div class="alert alert-danger hidden" id="MessageNotSent">
								Oops! Something went wrong please refresh the page and try again.
							</div>
							<form role="form" id="insertform" class="margin-clear insRegistration">
                                                            
										<div class="form-group has-feedback">
											<label for="name3">scholar Number</label>
											<input type="text" class="form-control" name="sch_number" id="sch_number" placeholder="" >
											<i class="fa fa-user form-control-feedback"></i>
										</div>
										<div class="form-group has-feedback">
											<label for="name3">student Name</label>
											<input type="text" class="form-control" name="sname" id="sname" placeholder="" >
											<i class="fa fa-user form-control-feedback"></i>
										</div>
										<div class="form-group has-feedback">
											<label for="fathers name">Fathers Name</label>
											<input type="text" class="form-control" name="fname" id="fname" placeholder="" >
											<i class="fa fa-envelope form-control-feedback"></i>
										</div>
										
										<div class="form-group has-feedback">
											<label>Father Contact</label>
                                                                                        <input type="text" class="form-control" name="fcontact" id="fcontact" maxlength="10" placeholder="" >
                                                                                        
                                                                                        <i class="fa fa-phone form-control-feedback"></i>
                                                                                </div>
										<div class="form-group has-feedback">
											<label>Class</label>
                                                                                        <input type="text" class="form-control" name="class" id="class"  placeholder="" >
                                                                                        <i class="fa fa-phone form-control-feedback"></i>
                                                                                </div>
										<div class="form-group has-feedback">
											<label>Date of birth</label>
                                                                                        <input type="text" class="form-control" name="date" id="date" placeholder="" >
                                                                                         <i class="fa fa-pencil form-control-feedback"></i>
                                                                                </div>
										<div class="form-group has-feedback">
											<label>Password</label>
                                                                                        <input type="password" class="form-control" name="password" id="password" placeholder="" >
                                                                                         <i class="fa fa-pencil form-control-feedback"></i>
                                                                                </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                     <div class="col-md-3">
                                                                         <input type="hidden" name="pic_path" id="pic_path">
                                                                                    <div class="form-group">
                                                                                        <label for="image">Upload Image</label>
                                                                                        <div class="box-body">
                                                                                            <div class="uploadArea" id="dragAndDropFiles" style="min-height: 60px;">
                                                                                                <h1 style="font-size:14px;">Drop Images Here</h1>
                                                                                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="ipath" style="width: 0px; height: 0px; overflow: hidden;">

                                                                                            </div>
                                                                                        </div>
                                                                                        <p class="help-block"></p>
                                                                                    </div>

                                                                                </div> 
                                                                </div>
                                                                
                                                            </div>
                                                                                
                                                                                <br/>
										<input type="submit" value="Submit" class="submit-button btn btn-default">
									</form>
						</div>
						<!-- main end -->

						<!-- sidebar start -->
						<!-- ================ -->
						<aside class="col-md-3 col-lg-offset-1">
							<div class="sidebar">
								<div class="side vertical-divider-left">
									<h3 class="title logo-font"> <span class="text-default">Siddhi Vinayak Public School</span></h3>
									<div class="separator-2 mt-20"></div>
									<ul class="list">
										<li><i class="fa fa-home pr-10"></i>Near New SOS Children Village, Peeria Mohalla, Amzara Road Kajuri Kalan Bhopal.(M.P.)India</li>
										<li><i class="fa fa-phone pr-10"></i><abbr title="Phone">P:</abbr> 0755 290067</li>
										<li><i class="fa fa-mobile pr-10 pl-5"></i><abbr title="Phone">M:</abbr>  9893575900</li>
										
									</ul>
									
									<div class="separator-2 mt-20 "></div>
									
								</div>
							</div>
						</aside>
						<!-- sidebar end -->
					</div>
				</div>
			</section>
			<!-- section end -->
                    
                   
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                        
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
<script> 
    
    var obj_class ;
    var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "insertform", // Form ID
                        dragArea: "dragAndDropFiles", // Upload Area ID
                        multiUpload:"multiUpload",
                        url:"../server/controller/insertImage_student.php",
                        //onlad:onlad,
                        strlenght:25,
                        module:1
                        }
                        $(document).ready(function (){
                            if(obj_class==null){
                           obj_class=new $.UploadImg(obj);           
                        }
                        });
                        
jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {	
                       
                             var inputId = $("#"+this.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
//                    e.stopPropagation(); 
//                    e.preventDefault();
//                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <2){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
//                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
//                                that._deleteFiles($(this).parent().parent().attr('rel'));
//                                $(this).parent().parent().remove();
//                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                
                                if(that.up_obj.upd_button==null){
                                   
                                }
                                
                            }
                            $("#"+this.up_obj.dragArea).empty();
                            
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 1 files.")
                            }
                        }
                     
                        
                this.setStream = function(stream,iid){
                    that.all[iid]["byte_stream"]=stream;
                    $(".dfiles[rel='"+iid+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                   if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>')
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        
                      var image = {};  
                      if(that.up_obj.upd_button==null){
                          $('#insertform').find(":input").each(function() {
                            
                            if($(this).attr("name")=="icat"){
                                image["icat"]= $(this).val();
                            }
                           
                           
                        });
                          }else{
                              image['iid']=that.up_obj.iid;
                          }
                        
                      
                      image["ipath"] =file["byte_stream"] ;
                            $.ajax({
                            type:"post",
                            url:that.up_obj.url,
                            data:image,
                            success: function(data){
                            $("#"+obja.form).find("#pic_path").val(data.trim());
                              that.all={};
                                    } 
                                });
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
   
                this._startUpload = function(){
                    if(that.up_obj.upd_button==null){
                        
                            var reg = /^\\d+$/;
                            var b=true;
                            $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="icat" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill  Category").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                    
                            });

                            if(b==false){
                            return b;
                            }
                            if(Object.keys(that.all).length==0){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please upload  Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        return false;
                            }else{
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }else{
                            if(Object.keys(that.all).length==1){
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }
                    
                    
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,iid) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,iid),iid)
              that._startUpload();
              }
            };
        }

                this.readfiles=function(files,iid) {
                    that.processfile(files,iid); // process each file at once
                }

                this.resizeMe=function (img,iid) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[iid]["image"]=canvas1;
             
                  var canvas2 = document.createElement('canvas');
              canvas2.width =100;
              canvas2.height = 100;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 100, 100);
                                $("#"+this.up_obj.dragArea).append(canvas2);
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});

</script>

</html>



