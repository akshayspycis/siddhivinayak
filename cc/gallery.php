<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Gallery</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg clearfix" id="aboutr1">
				<div class="container">
					<div class="tab-content clear-style">
								<div class="tab-pane active" id="pill-1">						
									<div class="row masonry-grid-fitrows grid-space-10">
                                                                            <div id="data">
<!--                                                                                 <div class="col-md-3 col-sm-6 masonry-grid-item">
											<div class="listing-item white-bg bordered mb-20">
												<div class="overlay-container">
													<img src="../style/images/product-1.jpg" alt="">
													<a class="overlay-link popup-img-single" href="../style/images/product-1.jpg"><i class="fa fa-search-plus"></i></a>
													
													
												</div>
												
											</div>
										</div>-->
                                                                            </div>
                                                                           
										
										
									</div>
								</div>
								
							</div>
					
				</div>
			</section>
			<!-- section end -->
                    
                   
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                  <?php include './includes/querybanner.php';?>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
        <script type="text/javascript">
                    news={}
                    $(document).ready(function(){
                       
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/selectImage.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                       $.each(duce, function (index, article) {
                           var img= "../server/controller/"+article.ipath;
                            $("#data").append($("<div></div>").addClass("col-md-3 col-sm-6 masonry-grid-item") 
                                    .append($("<div></div>").addClass("listing-item white-bg bordered mb-20")
                                    .append($("<div></div>").addClass("overlay-container")
                                    .append("<img src= "+img+" />")
                                    .append($("<a></a>").addClass("overlay-link popup-img-single").attr("href",img)
                                    .append($("<i></i>").addClass("fa fa-search-plus"))
                                    )
                                    )
                                    )
                                    
                     );
                        });
                    }
                });
              
            

                    });  

                </script>
               
<!-- Developed by Infopark  -->
</html>
