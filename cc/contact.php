<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Contact</title>
		<!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                            	<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-8 space-bottom">
                                                    <p id="hpara"><b>It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</b></p>
                                                    <p>&nbsp;</p>
							<div class="alert alert-success hidden" id="MessageSent">
								We have received your message, we will contact you very soon.
							</div>
							<div class="alert alert-danger hidden" id="MessageNotSent">
								Oops! Something went wrong please refresh the page and try again.
							</div>
							<form role="form" id="" class="margin-clear insQueries">
										<div class="form-group has-feedback">
											<label for="name3">Name</label>
											<input type="text" class="form-control" name="qname" id="qname" placeholder="" >
											<i class="fa fa-user form-control-feedback"></i>
										</div>
										<div class="form-group has-feedback">
											<label for="email3">Email address</label>
											<input type="email" class="form-control" name="qemail" id="qemail" placeholder="" >
											<i class="fa fa-envelope form-control-feedback"></i>
										</div>
										<div class="form-group has-feedback">
											<label>Contact</label>
                                                                                        <input type="text" class="form-control" name="qcontact" id="qcontact" maxlength="10" placeholder="" >
                                                                                        <i class="fa fa-phone form-control-feedback"></i>
                                                                                </div>
										<div class="form-group has-feedback">
											<label>Subject</label>
											<input type="text" class="form-control" name="qsubject" id="qsubject" placeholder="" >
                                                                                         <i class="fa fa-pencil form-control-feedback"></i>
                                                                                </div>
										<div class="form-group has-feedback">
											<label for="message3">Message</label>
											<textarea class="form-control" rows="4" name="qmessage" id="qmessage" placeholder=""></textarea>
											<i class="fa fa-pencil form-control-feedback"></i>
										</div>
										<input type="submit" value="Submit" class="submit-button btn btn-default">
									</form>
						</div>
						<!-- main end -->

						<!-- sidebar start -->
						<!-- ================ -->
						<aside class="col-md-3 col-lg-offset-1">
							<div class="sidebar">
								<div class="side vertical-divider-left">
									<h3 class="title logo-font"> <span class="text-default">Siddhi Vinayak Public School</span></h3>
									<div class="separator-2 mt-20"></div>
									<ul class="list">
										<li><i class="fa fa-home pr-10"></i>Near New SOS Children Village, Peeria Mohalla, Amzara Road Kajuri Kalan Bhopal.(M.P.)India</li>
										<li><i class="fa fa-phone pr-10"></i><abbr title="Phone">P:</abbr> 0755 290067</li>
										<li><i class="fa fa-mobile pr-10 pl-5"></i><abbr title="Phone">M:</abbr>  9893575900</li>
										
									</ul>
									
									<div class="separator-2 mt-20 "></div>
									
								</div>
							</div>
						</aside>
						<!-- sidebar end -->
					</div>
				</div>
			</section>
			<!-- section end -->
                    
                   
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                        
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
</html>



