<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Admission</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg clearfix" id="aboutr1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Admission  at Siddhi Vinayak Public School</h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara">1. Admission is restricted to the seats available. </p> 
                                        <p id ="hpara">2. All application for admission must be made on the prescribed form. Application for K.G. I class must be submitted on the date Indicated on the school notice board. </p>
                                        <p id ="hpara">3. There are no regular registration for other classes and admission to such classes are made only when vacancies occur. </p>
                                        <p id ="hpara">4. Admission form must be duly filled by the parents supported by relevant document as desired by the school within the stipulated period. Failure to do so shall be a valid reason for cancellation/ rejection of the pupil's name form the list. </p>
                                        <p id ="hpara">5. All documents become the property of the school and are not returnable. Hence Photostat copies of the document duly attested by a competent authority must be submitted by and not the original. Parents are required to accompany the candidates for personal interview. </p>
                                        <p id ="hpara">6. Admissions are done strictly on the merit of the pupils. The child will be tested on the knowledge of the class he/she is studying at the time of the entrance test. </p>
                                        <p id ="hpara">7. A pupil who Joins directly from home or form any other school has to produce his/her official Birth Certificate/Transfer Certificate in support of the date of birth entered in the admission form. </p>
                                        <p id ="hpara">8. A pupil joining form a recognized school will not be admitted unless his/her Transfer Certificate form the previous school attended is produced. </p>
                                        <p id ="hpara">9. At the time of Admission, the parents/Guardian will submit four postage stamp size co lour photographs of child. Each student will be supplied an identity card after getting confirmation deposit fees about their admission. Students are instructed to safely preserve the card till the completion of the course.</p>
                                       <br>
					
				</div>
			</section>
			<!-- section end -->
                    
                        <section class="pv-30 light-gray-bg clearfix" id="aboutr1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Withdrawal </h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara">There will be no refund of fees incase of withdrawal/cancellation of seat of the candidate not joining the course after the payment. Transfer certificate will be issued only when all the dues of the school are settled.</p> 
                                        <p>&nbsp;</p>
                                         <p>&nbsp;</p>
                                        <h3 class="title logo-font text-center text-default">School Fee :   </h3>
                                        <div class="separator"></div>
                                        
                                        <p id ="hpara">1. Admission fee is to be paid at the time of admission </p>
                                        <p id ="hpara">2. Fees once deposit will be non-refundable and non-transferable in any circumstance.  </p>
                                        <p id ="hpara">3. Monthly fees must be paid before 1st to 20th' of every month. Otherwise fine will be charged.  </p>
                                        <p id ="hpara">4. In case of non payment of fee till 28th, the student will not be allowed to attend the classes. </p>
                                        <p id ="hpara">5. Total dues of complete year will have to be deposited by the student if he/she seeks transfer to other school. </p>
                                        <p id ="hpara">6. The caution money is refundable without interest at the time of issuing the transfer certificate and will not be adjustable with any other fee. </p>
                                        <p id ="hpara">7. In addition to tution fees and admission fees each student should pay term fees/examination fees/board fees and ther fees of the school. </p>
                                        <p id ="hpara">8. The fees paid must be dually entered in the fee card without which no claim regarding payment of fees will be considered. </p>
                                        <p id ="hpara">9. The students will be allowed to appear in exam only after the issued of no dues certificate. </p>
                                        <p id ="hpara">10. The fees and bus fees is subjected to modification. </p>
                                        <p id ="hpara">11. Addition to annual and institution fees, each student should pay cultural fee,annual fee, sport fee and other fees of the school as applicable from time to time.  </p>
                                      
					<p>&nbsp;</p>
                                         <p>&nbsp;</p>
                                        <h3 class="title logo-font text-center text-default">School Time :     </h3>
                                        <div class="separator"></div>
                                        <p id="hpara">8:30 am to 1:45 pm Pre-Nursery & SW. - Age 2 years to 3 years Ilird to Vth std. Age 6 years to 8 years Age 9years to 11 years VIth to YOB h std. - Age 12 years to 14 years D I MI (RECOGNISED BY M.P. GOVT.) ESTD. 2004 SWUM Vinavak Public School (RECOGNISED BY M.P. GOVT.) DISE Code 23320304820 English 8 Hindi Medium Gram Kfrazurs Ratan. Bhopal. Mob. 9826058403. 9693575900 </p>
                                         <p>&nbsp;</p>
                                         <p>&nbsp;</p>
                                        <h3 class="title logo-font text-center text-default">Admission Eligibility    </h3>
                                        <div class="separator"></div>
                                       <table class="table table-bordered">
                                           <thead>
                                               <tr>
                                                   <th>Classes</th>
                                                   <th>Age</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>Pre-Nursery Nursery</td>
                                                   <td>Age 2 Years to 3 Years</td>
                                               </tr>
                                               <tr>
                                                   <td>Nursery, KG-I, KG-II</td>
                                                   <td>Age 3 Years to 5 Years years</td>
                                               </tr>
                                               <tr>
                                                   <td>Ist and IInd Std.</td>
                                                   <td>Age 6 Years to 8 Years</td>
                                               </tr>
                                               <tr>
                                                   <td>IIIrd and Vth Std.</td>
                                                   <td>Age 9 Years to 11 Years</td>
                                               </tr>
                                               <tr>
                                                   <td>VIth TO VIIth Std.</td>
                                                   <td>Age 12 Years to 14 Years</td>
                                               </tr>
                                           
                                           </tbody>
                                       </table>
                                        
                                        <br>
				</div>
			</section>
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                    
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			<?php include './includes/querybanner.php';?>
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
</html>



