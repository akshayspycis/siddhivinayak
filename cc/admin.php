<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>Registration</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>
         <?php
if (isset($_COOKIE['username']) && isset($_COOKIE['password']) ){
        $_SESSION['username'] = $_COOKIE['username'];
        $_SESSION['password'] = $_COOKIE['password'];
        $_SESSION['c_id'] = $_COOKIE['c_id'];
        $_SESSION['c_name'] = $_COOKIE['name'];
      
 ?>
  <script>
//      alert("<?php echo $_COOKIE["username"]; ?>");
//      alert("<?php echo $_COOKIE["password"]; ?>");
//      alert("<?php echo $_COOKIE["c_id"]; ?>");
//      alert("<?php echo $_COOKIE["name"]; ?>");
//      alert("<?php echo $_SESSION['c_name']; ?>");
     $(location).attr('href','shop.php');
     
 </script>
 <?php 
}else if(isset($_SESSION["c_id"]) || (isset($_COOKIE['username']) && isset($_COOKIE['password']))){
    ?>
 <script>
//     alert("<?php echo $_SESSION["c_id"]; ?>");
     $(location).attr('href','shop.php');
 </script>
<?php
}
?>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                            	<div class="main-container dark-translucent-bg" style="background-image:url('../style/images/slide-img3.JPG');">
				<div class="container">
					<div class="row">
						<!-- main start -->
						<!-- ================ -->
						<div class="main object-non-visible animated object-visible fadeInUpSmall" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
							<div class="form-block center-block p-30 light-gray-bg border-clear">
								<h2 class="title">Login</h2>
                                                                <form class="form-horizontal" id="Login">
									<div class="form-group has-feedback">
										<label for="inputUserName" class="col-sm-3 control-label">Scholar Number.</label>
										<div class="col-sm-8">
                                                                                    <input type="text" class="form-control" id="c_contact" placeholder="Scholar Number" name="c_contact" >
											<i class="fa fa-user form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputPassword" class="col-sm-3 control-label">Password</label>
										<div class="col-sm-8">
                                                                                    <input type="password" class="form-control" id="c_password" placeholder="Password" name="c_password" >
											<i class="fa fa-lock form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-8">
											<button type="submit" class="btn btn-group btn-default btn-animated">Log In <i class="fa fa-user"></i></button>
											
										</div>
									</div>
								</form>
							</div>
							
						</div>
						<!-- main end -->
					</div>
				</div>
			</div>
			
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->
                <div id="confirm" class="modal fade bs-example-modal-sm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
                                                                                    <h4 class="modal-title"   id="mySmallModalLabel">Alert !</h4>
                                                                                </div>
										<div class="modal-body">
											<p id="showid"></p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-sm btn-dark" data-dismiss="modal" id="ok_model">Ok</button>
										</div>
									</div>
								</div>
							</div>
		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
        <script type="text/javascript">
            $('#Login').submit(function() {
             var userfilter = /^\d{10}$/;
             var passfilter = /^\d{10}$/;
                           if(this.c_password.value == "" || !passfilter.test(this.c_password.value)){
                               $("#confirm").modal('show');
                                 $("#confirm").on('shown.bs.modal', function(){
                                       $("#showid").text("Please fill valid password.") 
                                   });
                                    return false;
                           }else{
                            $.ajax({
                                type:"post",
                                url:"../server/controller/Login.php",
                                data:$('#Login').serialize(),
                                success: function(data){
                                    var result = $.trim(data);
                                    if(result==="admin"){
                                        $(location).attr('href','../ac/home.php');
                                    }else if(result!="error"){
                                        $(location).attr('href','account.php?id='+result);  
                                    }else{
                                        $('#Login').each(function(){
                                            this.reset();
                                        });
                                        $("#confirm").modal('show');
                                        $("#confirm").on('shown.bs.modal', function(){
                                           $("#showid").text("Invalid User Id & Password") 
                                        });
                                    }
                                }
                            });
                           }
                    return false;
    });
        </script>
<!-- Developed by Infopark  -->
</html>



