<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | About Us</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg clearfix" id="aboutr1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Siddhi Vinayak Public School</h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara">Education primarily points to a challenge, inviting an individual to engage in a process to create meaning in one's life. The vision of <b>Siddhi Vinayak Public School </b>challenges individuals to engage in this process creatively and imbibe this simple outlook of life. The students are provided with abundant experiences of insightful living by observing different hues of life that make their life thoughtful and resourceful. Learning more than ever before, is a challenge to remain self-reflective, imaginative and motivated. The curriculum that one encounters as a student in this abode of learning, offer the possibilities to transcend beyond the ordinary perceptions of life. We believe this enhancement of life, initiate a desire to remain steadfast to learning and seek understanding of life which brings in more light in one's life to engage in nation building tomorrow. 
                                        <p id ="hpara">Students in Siddhi Vinayak Public School are constantly reminded that every event or person, however insignificant they may be, can impart a lesson dear to life. We believe this openness to life would make life's journey matchless game to be enjoyed in one's life. We hope, every day a child spends at this temple of learning would broaden the horizon of his or her to meet the crossroads of life with passion, vision and commitment towards a brighter future for oneself without ignoring the nation and every other human being.</p>
                                        <p class="text-center" id="hpara"><b>Siddhi Vinayak Public School</b> is an institution dedicated to impart quality education. Our aim is to make the student excellent performers with utmost self-confidence and practical wisdom, so as to be successful in whatever field of activities they get involved after education. The education should inculcate in student's strength, confidence, discipline, character and achieve higher values of life. We provide all facilities to develop all round personality and leadership development of our students through academics, sports and co-curricular activities. </p>
					
					<br>
					
				</div>
			</section>
			<!-- section end -->
                        <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
			<!-- section start -->
			<!-- ================ -->
                    
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			<section class="section clearfix" id="hrow4">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3>Our <span class="text-default"> Vision</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
							<p id="hpara">We are committed to provide educational excellence for all.</p>
								
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Our <span class="text-default"> Mission</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
							<p id="hpara">We provide the highest quality education so that all of our students are empowered to lead 
productive and fulfilling lives as lifelong learners and responsible citizens.
</p>
								
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Our <span class="text-default">Values</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox" style="height:305px">
                                                    <p id="hpara">We, the faculty and staff of the SVPS of Education, commit to the following values:  </p>
                                                    <p id="hpara"><b>:: Leadership</b><br> <b>:: Excellence</b><br>
                                                    <b>:: Integrity</b><br> <b>:: Citizenship</b></p>
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Campus <span class="text-default">Gallery</span></h3>
							<div class="separator-2"></div>
							<div class="owl-carousel content-slider-with-large-controls-autoplay dark-controls light-gray-bg buttons-hide" id="hbox">							
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus0.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left"></p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus1.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Practical Lab</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus2.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Library</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus3.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Play Ground</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			<?php include './includes/querybanner.php';?>
			<!-- section end -->
			
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
</html>


