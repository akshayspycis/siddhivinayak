<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>Siddhi Vinayak Public School</title>
		
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
               
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow" >
					
					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container"> 
						<div class="slider-banner-fullwidth-big-height" >
							<ul class="slides" id="homeslider">
								<!-- slide 1 start -->
								<!-- ================ -->
								<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="">
								<img src="../style/images/s-slider-slide-1.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
								</li>
								<!-- slide 1 end -->

								<!-- slide 2 start -->
								<!-- ================ -->
								<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="">
								
								<!-- main image -->
                                                                <img src="../style/images/s-slider-slide-2.jpg" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">

								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="600"
									data-start="0">
								</div>
                                                            </li>
								<!-- slide 2 end -->
							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg clearfix" id="hrow1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Siddhi Vinayak Public School</h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara"><b>Siddhi Vinayak Public School</b> is an institution dedicated to impart quality education. Our aim is to make the student excellent performers with utmost self-confidence and practical wisdom, so as to be successful in whatever field of activities they get involved after education. The education should inculcate in student's strength, confidence, discipline, character and achieve higher values of life. We provide all facilities to develop all round personality and leadership development of our students through academics, sports and co-curricular activities. </p>
					
					<br>
					<div class="row grid-space-10">
						<div class="col-sm-6 col-md-3">
							<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox">
								<span class="icon default-bg circle"><i class="fa fa-book"></i></span>
								<h3>Our Teachers</h3>
								<div class="separator clearfix"></div>
								<p id="hpara">Our teachers promote the development of attitudes and values within a curriculum and teaching environment that supports independent learning, confidence and global understanding.
We are aware that one of the most important people in your child’s life is their teacher.</p>
                                                                <p>&nbsp;</p>
                                                                <p>&nbsp;</p>
								
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="pv-30 ph-20 white-bg feature-box bordered  text-center" id="hbox">
								<span class="icon default-bg circle"><i class="fa fa-book"></i></span>
								<h3>Our Concept</h3>
								<div class="separator clearfix"></div>
								<p id="hpara">The School curriculam teaching methodology teacher student interaction inputs are based on the latest thoughts and trends is education. The School will be run on lines of modern principles in managment science especially emphasizing upon the importance of human resources.</p>
								<p>&nbsp;</p>
                                                                <p>&nbsp;</p>
							</div>
						</div>
                                            <div class="col-sm-6 col-md-3">
							<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox">
								<span class="icon default-bg circle"><i class="fa fa-building-o"></i></span>
								<h3>Our Campus</h3>
								<div class="separator clearfix"></div>
								<p id="hpara">School has excellent infrastructure and pleasing natural environment conducive to effective learning. The building is magnificent and spacious with open space and greenery all around, making the campus environment pleasant, peaceful and beautiful. The school currently has a well furnished building at Khaiuri kaian, Peria Mohalla, near New SOS balgram,Bypass Road, Bhopal</p>
								
							</div>
						</div>
						
						<div class="col-sm-6 col-md-3">
							<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox">
								<span class="icon default-bg circle"><i class="fa fa-bus"></i></span>
								<h3>Transport Service</h3>
								<div class="separator clearfix"></div>
								<p id="hpara">We provide Bus facility to the students, our bus runs on most every route of Bhopal city.The school provides transport facility to its students as per the existing route. Parents are requested to note that bus routes are planned to suit the convenience of the majority who use the bus, hence routes cannot be changed or extended to cater to individual requirements at the cost of time and distance.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->
                        <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
			<!-- section start -->
			<!-- ================ -->
                      
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			<section class="section clearfix" id="hrow4">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h3>Latest <span class="text-default">News & Events</span></h3>
							<div class="separator-2"></div>
							<div class="block">
                                                                  <marquee behavior="scroll" direction="up" onmouseover="this.stop();" onmouseout="this.start();"  scrollamount="2" loop="true" style="height:300px">
                                                                      <div id="data">
<!--                                                                          <div class="media margin-clear">
                                                                              <div class="media-left">
                                                                                  <div class="overlay-container">
                                                                                      <img class="media-object" src="../style/images/announcements.png" alt="blog-thumb">
                                                                                          
                                                                                  </div>
                                                                              </div>
                                                                              <div class="media-body">
                                                                                  <h5 class="media-heading" id="newshead">First Gold Cup Winner in Taekwondo Competition</h5>
                                                                                  <p class="small"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
                                                                                  <p class="margin-clear small" id="hpara">Students of  Siddhi Vinayak Public School, Bhopal participated in the “First Gold Cup Taekwon-Do” Competition organized by Lakshya Taekwon-Do Club, Bhopal on August 17, 2016 at Jawahar Lal Nehru Stadium, Bhopal.</p>
                                                                                      
                                                                              </div>
                                                                              <hr>
                                                                          </div>
                                                                           <div class="separator-2"></div>-->
                                                                      </div>
                                                                     
                                                               
<!--                                                                <div class="media margin-clear">
									<div class="media-left">
										<div class="overlay-container">
                                                                                    <img class="media-object" src="../style/images/announcements.png" alt="blog-thumb">
											<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
										</div>
									</div>
									<div class="media-body">
										<h5 class="media-heading" id="newshead">First Gold Cup Winner in Taekwondo Competition</h5>
										<p class="small"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
										<p class="margin-clear small" id="hpara">Students of  Siddhi Vinayak Public School, Bhopal participated in the “First Gold Cup Taekwon-Do” Competition organized by Lakshya Taekwon-Do Club, Bhopal on August 17, 2016 at Jawahar Lal Nehru Stadium, Bhopal.</p>
										
									</div>
									<hr>
								</div>
                                                                <div class="separator-2"></div>-->
                                                            </marquee>
                                                     



								
								
							</div>
						</div>
						<div class="col-md-3">
							<h3>Our <span class="text-default"> Motto</span></h3>
							<div class="separator-2"></div>
							<!-- accordion start -->
							<!-- ================ -->
						<div class="pv-30 ph-20 white-bg feature-box bordered text-center" id="hbox">
							<p id="hpara">‘Pranodevi Saraswati’, the famous Sanskrit adage is the motto of our Learning Institution, the Siddhi Vinayak Public School. According to Indian  mythology, Saraswati is the Goddess of Wisdom. Queens pray to become one with Her Spirit of Learning  and promise unto themselves that they  shall keep acquiring knowledge and wisdom.</p>
								
                                                </div>	
							<!-- accordion end -->
						</div>
						<div class="col-md-3">
							<h3>Campus <span class="text-default">Gallery</span></h3>
							<div class="separator-2"></div>
							<div class="owl-carousel content-slider-with-large-controls-autoplay dark-controls light-gray-bg buttons-hide">							
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus0.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left"></p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus1.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Practical Lab</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus2.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Library</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<div class="image-box style-2">
									<div class="overlay-container overlay-visible">
                                                                            <img src="../style/images/campus3.png" alt="">
										
										<div class="overlay-bottom hidden-xs">
											<div class="text">
												<p class="lead margin-clear text-left">Play Ground</p>
											</div>
										</div>
									</div>
									<br/>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->
  
			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			<?php include './includes/querybanner.php';?>
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
         <script type="text/javascript">
                    news={}
                    $(document).ready(function(){
                       
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/selectNews.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            news[article.nid]={};
                            news[article.nid]["nheading"]=article.nheading;
                            news[article.nid]["ncontent"]=article.ncontent;
                            news[article.nid]["ndate"]=article.ndate;
                            news[article.nid]["nposted"]=article.nposted;
                            news[article.nid]["nimage"]=article.nimage;
                           var img= "../server/controller/"+article.nimage;
                            $("#data").append($("<div></div>").addClass("media margin-clear") 
                                    .append($("<div></div>").addClass("media-left") .append($("<div></div>").addClass("overlay-container").append("<img src= "+img+" />").addClass("media-object").attr("alt","blog-thumb")))
                                    .append($("<div></div>").addClass("media-body")
                                    .append($("<h5></h5>").addClass("media-heading").attr("id","newshead").append(article.nheading))
                                     .append($("<p></p>").addClass("small").append("<i></i>").addClass("fa fa-calendar pr-10").append(article.ndate))
                                     .append($("<p></p>").addClass("margin-clear small").attr("id","hpara").append(article.ncontent)))
                                     .append($("<p></p>").append("&nbsp;"))
                                     .append($("<div></div>").addClass("separator"))
                                     .append($("<p></p>").append("&nbsp;"))
                     );
                        });
                    }
                });
              
            

                    });  
                </script>
<!-- Developed by Infopark  -->
</html>
