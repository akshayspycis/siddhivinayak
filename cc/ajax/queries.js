$(document).ready(function() {
    
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('.insQueries').submit(function() { 
        
        if(this.qname.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                        $("#errormsg").find("#msg").append("Please choose valid 'Name'.") ;
                        
                     });
                     
                   setTimeout(function(){
                        $('#warningmsg').modal('hide');
                    }, 5000);
          this.qname.focus();
          return false;
        }
        else if (this.qemail.value == "" || !emailfilter.test(this.qemail.value)) {
             $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $("#errormsg").find("#msg").empty();  
                        $("#errormsg").find("#msg").append("Please enter valid 'Email'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);
          
          this.qemail.focus();
          return false;
        }
        else if(this.qcontact.value == "" || !contactnofilter.test(this.qcontact.value)) {
             $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $("#errormsg").find("#msg").empty();  
                        $("#errormsg").find("#msg").append("Please enter valid 'Contact No'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);
          this.qcontact.focus();
          return false;
        }
        else if (this.qsubject.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $("#errormsg").find("#msg").empty();  
                        $("#errormsg").find("#msg").append("Please enter valid 'Subject'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000); 
          
          this.qsubject.focus();
          return false;
        }
        
        else if (this.qmessage.value == "") {
           $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $("#errormsg").find("#msg").empty();  
                        $("#errormsg").find("#msg").append("Please enter  'Message'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);  
         this.qmessage.focus();
          return false;
        }
        else {      
           
            $.ajax({
                type:"post",
                url:"../server/controller/insertQueries.php",
               data:$('.insQueries').serialize(),
                success: function(data){
                    $('#enquirysuccess').modal('show'); 
                     $("#enquirysuccess").on('shown.bs.modal', function(){
//                        $('#msg').append(data) ;
                    }); 
                   setTimeout(function(){
                        $('#enquirysuccess').modal('hide')
                                }, 10000);
                    $('.insQueries').each(function(){
                        this.reset();
                    });
                   
                }
               
            });
        }
        return false;
    });
});

