$(document).ready(function() {
//    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('.insRegistration').submit(function() { 
        if(this.sch_number.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                        $(this).find("#msg").text("Please Enter Scholar Number") ;
                        
                     });
                     
                   setTimeout(function(){
                        $('#warningmsg').modal('hide');
                    }, 5000);
          this.sch_number.focus();
          return false;
        }
       else if(this.sname.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                        $(this).find("#msg").text("Please choose valid 'Name'.") ;
                        
                     });
                     
                   setTimeout(function(){
                        $('#warningmsg').modal('hide');
                    }, 5000);
          this.sname.focus();
          return false;
        }
        else if (this.fname.value == "") {
             $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").text("Please enter valid 'Fathers Name'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);
          
          this.fname.focus();
          return false;
        }
        else if(this.fcontact.value == "" || !contactnofilter.test(this.fcontact.value)) {
             $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").text("Please enter valid 'Contact No'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);
          this.fcontact.focus();
          return false;
        }
        else if (this.class.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").text("Please enter  'Class'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000); 
          
          this.class.focus();
          return false;
        }
        
        else if (this.date.value == "") {
           $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").text("Please enter  'Dob in dd/mm/yyyy formate.'.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);  
         this.date.focus();
          return false;
        }
        else {
            $.ajax({
                type:"post",
                url:"../server/controller/insertRegistration.php",
               data:$('.insRegistration').serialize(),
                success: function(data){
                   $('#enquirysuccess').modal('show'); 
                     $("#enquirysuccess").on('shown.bs.modal', function(){
                        $(this).find('#msg').text(data) ;
                    }); 
                   setTimeout(function(){
                        $('#enquirysuccess').modal('hide')
                    },2000);
                   setTimeout(function(){
                        location.reload();
                    },500);
                    
                   
                }
               
            });
        }
        return false;
    });
});




