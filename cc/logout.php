<?php
/**
 * Created by Lalit Pastor.
 * User: Infopark
 * Date: 01/03/2015
 * Time: 2:46 AM
 */

session_start();//session is a way to store information (in variables) to be used across multiple pages.

session_destroy();

header("Location: index.php");//use for the redirection to some page
?>