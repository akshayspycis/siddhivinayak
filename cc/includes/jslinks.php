<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
                 
		<!-- Jquery and Bootstap core js files -->
                <script type="text/javascript" src="../style/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="../style/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="../style/plugins/modernizr.js"></script>

		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="../style/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="../style/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                <!-- Isotope javascript -->
		<script type="text/javascript" src="../style/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="../style/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="../style/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="../style/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="../style/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="../style/plugins/jquery.validate.js"></script>

		<!-- Google Maps javascript -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;signed_in=true"></script>
		<script type="text/javascript" src="../style/js/google.map.config.js"></script>

		<!-- Background Video -->
		<script src="../style/plugins/vide/jquery.vide.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="../style/plugins/owl-carousel/owl.carousel.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="../style/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="../style/plugins/SmoothScroll.js"></script>
                    <!-- Initialization of Plugins -->
		<script type="text/javascript" src="../style/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="../style/js/custom.js"></script>
		<!-- Color Switcher (Remove these lines) -->
                <script type="text/javascript" src="../style/style-switcher/style-switcher.js"></script>
                <script type="text/javascript" src="ajax/queries.js"></script>
                <script type="text/javascript" src="ajax/registration.js"></script>
                <script type="text/javascript">
            $('#footerform').submit(function() {
                                     var email2filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     if(this.name2.value == ""){
                                         $(".validate_contact").addClass("text-danger").fadeIn(100).text("Please fill valid User name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#footerform').find('#name2').focus();
                                        return false;
                                     }
                                     else if(this.email2.value == "" || !email2filter.test(this.email2.value)){
                                         $(".validate_contact").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#footerform').find('#email2').focus(); 
                                         return false;
                                     }
                                     else if(this.message2.value == ""){
                                         $(".validate_contact").addClass("text-danger").fadeIn(100).text("Please fill valid Message").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#footerform').find('#message2').focus();
                                         return false;
                                         
                                     }
                                     else{
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/InsEnquiry.php",
                                        data:$('#footerform').serialize(),
                                        success: function(data){  
                                            $("#showid_contact").empty();
                                            $("#showid_contact").append(data);
                                            $('#contact_confirm').modal('show');
                                            
                                            } 
                                        });
                                   return false;   
                                  }
                                     
                    });
                    $("#ok_model").click(function() {
                        location.reload(true);
                    });
        </script>
        <div class="modal fade in" id="warningmsg" role="dialog"  aria-hidden="false">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Error !</h4>
        </div>
        <form id="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden" name="iid" id="iid" value="11" class="form-control">
        <p id="msg"></p>
         </div>
        </div>
        <div class="modal-footer">
      
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
        <div class="modal fade in" id="enquirysuccess" role="dialog"  aria-hidden="false">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Success</h4>
        </div>
        <form id="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden" name="iid" id="iid" value="11" class="form-control">
        <p id="msg"></p>
         </div>
        </div>
        <div class="modal-footer">
      
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
        