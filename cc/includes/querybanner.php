<section class="section dark-bg clearfix" id="hrow3">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
									<div class="col-sm-10">
										<h1 class="title"><i class="fa fa-book pr-10"></i>Come to Learn</h1>
										<ul class="list-inline">
											<li><i class="icon-dot text-default"></i> <a href="#" class="link-light">Pre-Nursery Nursery</a></li>
											<li><i class="icon-dot text-default"></i> <a href="#" class="link-light">Nursery, KG-I, KG-II</a></li>
											<li><i class="icon-dot text-default"></i> <a href="#" class="link-light">Ist and IInd Std.</a></li>
											<li><i class="icon-dot text-default"></i> <a href="#" class="link-light">IIIrd and Vth Std.</a></li>
											<li><i class="icon-dot text-default"></i> <a href="#" class="link-light">VIth TO VIIth Std</a></li>
										</ul>
									</div>
									<div class="col-sm-2">
										<br>
                                                                                <p><a href="contact.php" class="btn btn-lg btn-default btn-animated">Admission<i class="fa fa-send pl-20"></i></a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>