
<footer id="footer" class="clearfix ">

				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" id="foot">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-8">
									<div class="footer-content">
                                                                            <h3 title logo-font text-center text-default><span id="foothead"><b>SVPS</b> - Born Out of a Dream </span></h3>
										<div class="row">
											<div class="col-md-6">
												<p id="hpara">"Just like every parent we aspired to give the best to our children, best of the education, best of our attention without any compromise. It was a wish, a longing and a dream. SVPS is that dream come true..." </p>
												
												<ul class="list-icons">
													<li><i class="fa fa-map-marker pr-10 text-default"></i>Near New SOS Children Village, Peeria Mohalla, Amzara Road Kajuri Kalan Bhopal.(M.P.)</li>
													<li><i class="fa fa-phone pr-10 text-default"></i> 0755 290067</li>
													<li><i class="fa fa-phone pr-10 text-default"></i> 9893575900</li>
													
												</ul>
											</div>
											<div class="col-md-6">
												
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="footer-content">
										<h2 class="title">Contact Us</h2>
										<br>
										<div class="alert alert-success hidden" id="MessageSent2">
											We have received your message, we will contact you very soon.
										</div>
										<div class="alert alert-danger hidden" id="MessageNotSent2">
											Oops! Something went wrong please refresh the page and try again.
										</div>								
										<form role="form" id="footerform" class="margin-clear">
											<div class="form-group has-feedback">
												<label class="sr-only" for="name2">Name</label>
												<input type="text" class="form-control" id="name2" placeholder="Name" name="name2">
												<i class="fa fa-user form-control-feedback"></i>
											</div>
											<div class="form-group has-feedback">
												<label class="sr-only" for="email2">Email address</label>
												<input type="email" class="form-control" id="email22" placeholder="Enter email" name="email2">
												<i class="fa fa-envelope form-control-feedback"></i>
											</div>
											<div class="form-group has-feedback">
												<label class="sr-only" for="message2">Message</label>
												<textarea class="form-control" rows="6" id="message2" placeholder="Message" name="message2"></textarea>
												<i class="fa fa-pencil form-control-feedback"></i>
											</div>
                                                                                        <div class="validate_contact pull-left" style="font-weight:bold;"></span></div>
											<input type="submit" value="Send" class="margin-clear submit-button btn btn-default pull-right">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="sfoot">
                                    <div id="footline"> </div>
                                         <div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center" id="hpara2">SVPS Copyright © 2016 | Developed by 
                                                                            Infopark |  All Rights Reserved.</p>
								</div>
							</div>
						</div>
					</div>
                                   
                                   
				</div>
				<!-- .subfooter end -->

        <div id="contact_confirm" class="modal fade bs-example-modal-sm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
                                                                                    <h4 class="modal-title"   id="mySmallModalLabel">Contact US</h4>
                                                                                </div>
										<div class="modal-body">
											<p id="showid_contact"></p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-sm btn-dark" data-dismiss="modal" id="ok_model">Ok</button>
										</div>
									</div>
								</div>
							</div>
			</footer>
