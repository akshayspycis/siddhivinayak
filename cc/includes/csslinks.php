                <meta name="" content="Sidhhi Vinayak Public School is an institution dedicated to impart quality education.">
		<meta name="" content="">
                <!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Favicon -->
                <link rel="shortcut icon" href="../style/images/fevicon.png">
                <link href="../style/css/custom.css" rel="stylesheet">
                <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
                <!-- Bootstrap core CSS -->
                <link href="../style/bootstrap/css/bootstrap.css" rel="stylesheet">
                <!-- Font Awesome CSS -->
		<link href="../style/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
                <!-- Fontello CSS -->
		<link href="../style/fonts/fontello/css/fontello.css" rel="stylesheet">
                <!-- Plugins -->
		<link href="../style/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="../style/plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="../style/css/animations.css" rel="stylesheet">
		<link href="../style/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="../style/plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="../style/plugins/hover/hover-min.css" rel="stylesheet">		
		<!-- the project core CSS file -->
		<link href="../style/css/style.css" rel="stylesheet" >
                <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="../style/css/skins/dark_cyan.css" data-style="styles-no-cookie" rel="stylesheet">
		<link href="../style/style-switcher/style-switcher.css" rel="stylesheet">

		<!-- Custom css --> 
		
                
                    