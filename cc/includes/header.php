
				<div class="header-top  colored">
					<div class="container">
						<div class="row">
							<div class="col-xs-2 col-sm-5">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix">
									<div id="header-top-second"  class="clearfix text-left">
									<nav>
										<ul class="list-inline">
                                                                                    <li class="hidden-sm hidden-xs"><strong class="pl-5">Call Us:</strong> <b>0755-2900674</b></li>
										</ul>
									</nav>
                                                                    </div>
									
								</div>
								<!-- header-top-first end -->
							</div>
							<div class="col-xs-10 col-sm-7">
<div class="btn-group pull-right">
    
    <?php
        if(isset($_SESSION["c_id"])){?><a href="logout.php" class="btn btn-default btn-sm"><i class="fa fa-user pr-10"></i>Sign Out</a>
    <?php    
      }  else {
                ?>
           <a href="admin.php" class="btn btn-default btn-sm"><i class="fa fa-user pr-10"></i> Sign In</a>
            <?php    
            }
      ?>
                                                                                    
										</div>
								<!-- header-top-second start -->
								<!-- ================ -->
								
								<!-- header-top-second end -->
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->
				
				<!-- header start -->
				<!-- classes:  -->
				<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
				<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
				<!-- "full-width": mandatory class for the full-width menu layout -->
				<!-- "centered": mandatory class for the centered logo layout -->
				<!-- ================ --> 
				<header class="header clearfix">
					
					<div class="container">
                                            <div class="row" >
                                                <div class="col-md-1" id="headbanner">
                                                    <img src="../style/images/logo2.png"  class="responsive" id="banimg"/>
                                                </div>
                                                <div class="col-md-10">
                                                    <h1 id="headheading" class="responsive">Siddhi Vinayak Public School</h1>
                                                    <p class="text-center" id="headpara">(Recognized by MP Govt. & Madhya Pradesh Board of Secondary Education)</p>
                                                </div>
                                                <div class="col-md-1" id="headbanner">
                                                    <img src="../style/images/logo4.png"  class="responsive" id="banimg"/>
                                                </div>
                                            </div>
                                            </div>
                                    <div id="wrap-nav">
                                         <div class="container">
						<div class="row">
							
							<div class="col-md-12">
					
								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation" >
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse" id="navbar-collapse-1">
												<!-- main-menu -->
                                                                                                <ul class="nav navbar-nav" id="nbar1">
                                                                                                    <li><a href="index.php">Home</a></li>
                                                                                                    <li><a href="about.php">About Us</a></li>
													<!-- mega-menu start -->
													
													<!-- mega-menu end -->
													
                                                                                                        <li><a href="facilities.php">Facilities</a></li>
                                                                                                        <li><a href="admission.php">Admission</a></li>
                                                                                                        <li><a href="gallery.php">Gallery</a></li>
                                                                                                        <li><a href="download.php">Downloads</a></li>
                                                                                                        <li><a href="registration.php">Registration</a></li>
                                                                                                        <li><a href="contact.php">Contact Us</a></li>
													
												</ul>
										</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->	
								</div>
								<!-- header-right end -->
					
							</div>
						</div>
					</div>
                                    </div>
                                   
					
				</header>
				<!-- header end -->

