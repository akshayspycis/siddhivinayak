<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<!-- Developed by Infopark -->
<html lang="en">
	<!--<![endif]-->
              <head>
		<meta charset="utf-8">
		<title>SVPS | Download</title>
                    <?php
                    $dbhost = "localhost";
                    $dbuser = "infopark_siddhiv";
                    $dbpass = 'WIy$T?Ip)8[(';
                    $dbname = "infopark_siddhivinayak";
                    $conn= mysqli_connect($dbhost,$dbuser,$dbpass) or die('cannot connect to the server'); 
                    mysqli_select_db($conn,$dbname) or die('database selection problem');
                    ?>
                <!-- Web Fonts -->
		<?php include './includes/csslinks.php'; ?>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
                <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
				<?php include './includes/header.php'; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
			
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>
                            <section class="pv-30 light-gray-bg clearfix" id="hrow1">
				<div class="container">
					<h3 class="title logo-font text-center text-default">Downloads</h3>
					<div class="separator"></div>
                                        <p class="text-center" id="hpara"><b>Here are the files to download</p>
					<div class="row">
                                                            <?php
                                                            $sql="SELECT * FROM tbl_uploads";
                                                            $result_set=mysqli_query($conn,$sql);
                                                            while($row=mysqli_fetch_array($result_set))
                                                            {
                                                            ?>
                                                           
                                                            <div class="col-md-12">
								<div class="call-to-action p-20 light-gray-bg shadow bordered">
									
									<div class="row">
										<div class="col-sm-8">
											<p id="downloadhead"><?php echo $row['file'] ?></p>
										</div>
										<div class="col-sm-4 text-right">
                                                                                    <a class="btn btn-lg btn-default btn-animated" href="../ac/uploads/<?php echo $row['file'] ?>" target="_blank">view file <i class="fa fa-download pl-20"></i></a>
										</div>
									</div>
								</div>
                                                            </div>
                                                            <p>&nbsp;</p>
                                                            <?php
                                                            }
                                                            ?>
							
						</div>
					
				</div>
			</section>
			<!-- section start -->
			<!-- ================ -->
                        
			<!-- section end -->
                   
                   
			<!-- section start -->
			<!-- ================ -->
                            <section class="full-width-section" id="hrow2">
                            
                            <div id="wrap-div">
                                <div class="container" id="dcontent">
                                    
                                    <h3 class="title logo-font text-center text-default">Inspiring Quotes</h3>
                                    <div class="separator"></div>
                                 <div class="owl-carousel space-bottom content-slider">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.</p>
										</blockquote>
                                                                            <div class="testimonial-info-1"><span id="hpara2">- Swami Vivekananda</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="testimonial text-center">
									<div class="testimonial-body">
										<blockquote>
											<p id="hpara2">We shall never know all the good that a simple smile can do.</p>
										</blockquote>
										 <div class="testimonial-info-1"><span id="hpara2">- Mother Teresa</span></div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
                                </div>
                                    
                            </div>
				
			</section>
                       
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
			
			<!-- section end -->
			<?php include './includes/querybanner.php';?>
			<!-- footer top start -->
			<!-- ================ -->
			<div class="" id="footupperstrip">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action text-center">
								<div class="row">
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
		<!-- Color Switcher End -->
	</body>
        <?php include './includes/jslinks.php'; ?>
<!-- Developed by Infopark  -->
</html>



