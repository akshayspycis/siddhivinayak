<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Send SMS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Send SMS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Send SMS</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    <?php 
                        $numbers = '';
                     if(isset($_GET["contact"])){
                         $numbers = $_GET["contact"];
                   }
                    
                    
                    ?>
                        <form id ="senSms" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            
                            <div class="form-group">
                            <label for="cn">Contact Number</label>
                            <textarea name ="numbers" id="numbers" class="form-control" rows="3" placeholder="" style="resize:none"><?php echo $numbers ?></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="msg">Message</label>
                                    <textarea name ="msg" id="msg" class="form-control" rows="3" value="" placeholder="Enter Message" style="resize:none"></textarea>
                                </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Send</button>
                            

                            </div>
                            </div>
                            </form>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
     <div class="modal fade in" id="warningmsg" role="dialog"  aria-hidden="false">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Error !</h4>
        </div>
        <form id="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden" name="iid" id="iid" value="11" class="form-control">
        <p id="msg"></p>
         </div>
        </div>
        <div class="modal-footer">
      
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
        <div class="modal fade in" id="enquirysuccess" role="dialog"  aria-hidden="false">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Success</h4>
        </div>
        <form id="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden" name="iid" id="iid" value="11" class="form-control">
        <p id="msg"></p>
         </div>
        </div>
        <div class="modal-footer">
      
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
    <!--Insert Category Insert Modal Start-->
    <script>
    $(document).ready(function() {
//    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#senSms').submit(function() { 
        
        if(this.numbers.value == "") {
            $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                        $(this).find("#msg").append("Please Enter Numbers") ;
                        
                     });
                     
                   setTimeout(function(){
                        $('#warningmsg').modal('hide');
                    }, 5000);
          this.numbers.focus();
          return false;
        }
        else if (this.msg.value == "") {
             $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").append("Please enter Message.") ;
                        
                     });
                   setTimeout(function(){
                       $('#warningmsg').modal('hide');
                    }, 5000);
          
          this.msg.focus();
          return false;
        }
        
        else {
            $.ajax({
                type:"post",
                url:"../server/controller/send.php",
               data:$('#senSms').serialize(),
                success: function(data){
                    if(data.trim().length==24){
                        $('#enquirysuccess').modal('show'); 
                        $("#enquirysuccess").on('shown.bs.modal', function(){
                            $(this).find('#msg').text("Your Message send successfully.") ;
                        }); 
                       setTimeout(function(){
                            $('#enquirysuccess').modal('hide')
                                    }, 10000);
                        $('#senSms').each(function(){
                            this.reset();
                        });
                    }else{
                        $('#warningmsg').modal('show'); 
                     $("#warningmsg").on('shown.bs.modal', function(){
                         $(this).find("#msg").empty();  
                        $(this).find("#msg").append("There is some internal server error.") ;
                        
                     });
                    }
                   
                   
                }
               
            });
        }
        return false;
    });
});





    
    </script>
  </body>
</html>



