 <?php
        session_start();
        if(isset($_SESSION["c_id"])){
                if($_SESSION["c_id"]!=1){
                ?>
           <script>
                $(location).attr('href','../cc/index.php');
           </script>
     <?php    
                }
            }  else {
                ?>
           <script>
                $(location).attr('href','../cc/index.php');
           </script>
            <?php    
            }
      ?>
<header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="style/images/user.png" class="user-image" alt="User Image" />
                  <span class="hidden-xs"> <?php echo $_SESSION['c_name'] ; ?> </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="style/images/user.png" class="img-circle" alt="User Image" />
                    <p>
                      <p>
                      <?php echo $_SESSION['c_name'] ; ?>
                    </p>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <span  data-toggle= "modal" id ="profile" data-target="#editprofile" class="btn btn-default btn-flat">Profile</span>
                    </div>
                    <div class="pull-right">
                        <a href="../cc/logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            
            </ul>
          </div>
        </nav>
      </header>
  <div class="modal fade" id="editprofile" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Customer Details </h4>
            </div>
             <form id ="updatecprofile" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="hidden" class="form-control" name="c_id" id ="c_id" value="">
                            <input type="text" class="form-control" name="c_name" id ="c_name" placeholder="Enter Customer  Name" disabled="true ">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="text" class="form-control"  name="c_dob" id ="c_dob" placeholder="Customer date of Birth">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="c_email" id ="c_email" placeholder="Customer Email">
                            </div>
                            <div class="form-group">
                            <label for="password">Address</label>
                            <textarea name ="c_address" id="c_address" class="form-control" rows="3" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="c_gender" name="c_gender">
                            <option value="Male" selected>Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="c_contact" name="c_contact" class="form-control" placeholder="Customer Contact" maxlength="10">
                            </div>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="c_password" name="c_password" class="form-control" placeholder="Customer Password" maxlength="4">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
                            </form>
            </div>
            </div>
  </div>   
<script type="text/javascript" language="javascript">
        /* Ajax call for Edit Customer */
         $(document).on("click", "#profile", function () {
            $("#editprofile").on('shown.bs.modal', function(){
                      $.ajax({
                        type:"post",
                        url:"../server/controller/userprofile.php",
                        success: function(data){
                           var duce = jQuery.parseJSON(data);
                            $.each(duce, function (index, article) {
                                 $("#editprofile").find("#c_id").val(article.c_id);  
                                    $("#editprofile").find("#c_name").val(article.c_name);  
                                    $("#editprofile").find("#c_dob").val(article.c_dob);  
                                    $("#editprofile").find("#c_contact").val(article.c_contact);  
                                    $("#editprofile").find("#c_password").val(article.c_password);  
                                    $("#editprofile").find("#c_email").val(article.c_email);  
                                    $("#editprofile").find("#c_address").val(article.c_address);
                             });
                                    
                                } 
                            });
                                    
                         $('#updatecprofile').submit(function() {
                         
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                      var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     
                                     if(this.c_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updatecprofile').find('#c_name').focus();
                                        return false;
                                       }else if(this.c_dob.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                           $('#updatecprofile').find('#c_dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_email.value == "" || !emailfilter.test(this.c_email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updatecprofile').find('#c_email').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.c_address.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updatecprofile').find('#c_address').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_gender.value == "" || !genderfilter.test(this.c_gender.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updatecprofile').find('#c_gender').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_contact.value == "" || !contactfilter.test(this.c_contact.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                         $('#updatecprofile').find('#c_contact').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_password.value == "" || !passwordfilter.test(this.c_password.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updatecprofile').find('#c_password').focus();
                                         return false;
                                         
                                     }
                                     else{  
                                        $.ajax({
                                        type:"post",
                                        url:"../server/controller/updateCustomer.php",
                                        data:$('#updatecprofile').serialize(),
                                        success: function(data){ 
                                        $('#updatecprofile').each(function(){
                                        this.reset();
                                        $('#editprofile').modal('hide');
                                        return false;
                                        });
                                        } 
                                        });
                                        return false;
                  }
                      
                   
    });
    return false;
    });
});
</script>