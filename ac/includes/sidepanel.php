  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="style/images/user.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Admin</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Main  Navigation</li>
            <li>
                <a href="home.php">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
             <li>
                <a href="registration.php">
                <i class="fa fa-files-o"></i>
                <span>Registration</span>
              </a>
             
            </li> 
             <li>
                <a href="send.php">
                <i class="fa fa-files-o"></i>
                <span>Send SMS</span>
              </a>
             
            </li> 
             <li>
                <a href="queries.php">
                <i class="fa fa-files-o"></i>
                <span>Queries</span>
              </a>
             
            </li> 
            <li class="treeview">
              <a href="newsupdate.php">
                <i class="fa fa-dashboard"></i> <span>News & Updates</span>
              </a>
            </li>
             <li class="treeview">
                <a href="gallerycate.php">
                    <i class="fa fa-files-o"></i>
                    <span>Gallery Categories</span>
                </a>
            </li>
            <li class="treeview">
              <a href="gallery.php">
                <i class="fa fa-dashboard"></i>
                <span>Gallery</span>
              </a>
            </li>
           
           
            <li>
                <a href="downloadsection.php">
                <i class="fa fa-files-o"></i>
                <span>Download Section </span>
              </a>
            </li> 
            <li>
                <a href="enquiry.php">
                <i class="fa fa-files-o"></i>
                <span >Enquiry</span>
              </a>
            </li> 
           
           
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>