<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          student={}
          $(document).ready(function(){
              onlad();
            });  
      </script>
    
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Registration</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Registration</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="sendAll">Send All</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Scholar Number</th>
                        <th>Student Name</th>
                        <th>Fathers Name</th>
                        <th>Contact</th>
                        <th>Class</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th>Password</th>
                       <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

    
  </body>

    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#sendAll", function () {
                 var number="";
                 $.each(student,function (key,value){
                     number+=value.fcontact+",";
                 })
                 window.location="send.php?contact="+number.substr(0, number.length-1);
});
  /* Code for Product Delete end */  
     
//...........................................................................................................

function onlad(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/selectRegistration.php",
                    success: function(data) {
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            student[article.id]={};
                            student[article.id]["fcontact"]=article.fcontact;
                            var img= "../server/controller/"+article.pic_path;
                        
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.sch_number))
                                .append($('<td/>').html(article.sname))
                                .append($('<td/>').html(article.fname))
                                .append($('<td/>').html(article.fcontact))
                                .append($('<td/>').html(article.class))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.password))
                                
                                
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.id+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editRegistration\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html('<a  class="btn btn-success"  href="send.php?contact='+article.fcontact+'")>Send SMS</a>'))
                             );
                        });
                    }
                });
                }
</script>


</html>


