<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Users</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
        <?php
        session_start();
        if(!isset($_SESSION["c_id"])){
                ?>
           <script>
                $(location).attr('href','../client/index.php');
           </script>
     <?php
            }
    ?> 
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
              onlad();
            });  
    </script> 
     
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
    <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
 <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Users</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addUsers">Add Users</button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Dob</th>
                        <th>Contact No.</th>
                        <th>Password</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Address</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        
                        <th><button class="btn btn-block btn-sm">Print &nbsp;&nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-print"></span></button></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>         
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
    <?php include 'includes/footer.php';?>
               <div class="modal fade" id="insCustomer" role="dialog">
                    <div class="modal-dialog"><!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Users </h4>
                            </div>
                            <form id ="insertCustomer" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="c_name" id ="c_name" value="" placeholder="Enter Customer  Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="date"  class="form-control"  name="c_dob" id ="c_dob" value="" placeholder="DD/MM/YYYY">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="c_email" id ="c_email" value="" placeholder="Customer Email">
                            </div>
                            <div class="form-group">
                            <label for="password">Address</label>
                            <textarea name ="c_address" id="c_address" class="form-control" rows="3"  value="" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="c_gender" name="c_gender">
                            <option value="" selected>Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="c_contact" name="c_contact" value="" class="form-control" placeholder="Customer Contact" maxlength="10">
                            </div>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="c_password" name="c_password" value="" class="form-control" placeholder="Customer Password" maxlength="4">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
                            </form>
                            </div>
                            </div>
                  </div> 
      
               <!--Modal For Update Customer Details-->
  <div class="modal fade" id="editcustomer" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Customer Details </h4>
            </div>
             <form id ="updateCustomer" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="hidden" class="form-control" name="c_id" id ="c_id" value="">
                            <input type="text" class="form-control" name="c_name" id ="c_name" placeholder="Enter Customer  Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="date" class="form-control"  name="c_dob" id ="c_dob"  placeholder="Customer date of Birth">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control"  name="c_email" id ="c_email" placeholder="Customer Email">
                            </div>
                            <div class="form-group">
                            <label for="password">Address</label>
                            <textarea name ="c_address" id="c_address" class="form-control" rows="3" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="c_gender" name="c_gender">
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="c_contact" name="c_contact" class="form-control"  maxlength="10" placeholder="Customer Contact">
                            </div>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="c_password" name="c_password" class="form-control" maxlength="10" placeholder="Customer Password">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                            

                            </div>
                            </div>
                            </form>
            </div>
            </div>
  </div>       
     <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Customer </h4>
        </div>
        <form id ="deleteCustomer">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="c_id" id="c_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
       
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
<script type="text/javascript" language="javascript">
    /*Code to insert Values of Users   */
      $(document).ready(function(){
        $("#addUsers").click(function(){
            $("#insCustomer").modal('show');}); 
                $("#insCustomer").on('shown.bs.modal', function(){
                            $('#insertCustomer').submit(function() {
                                   
                                     var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     
                                     if(this.c_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insertCustomer').find('#c_name').focus();
                                        return false;
                                       }else if(this.c_dob.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                           $('#insertCustomer').find('#c_dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_email.value == "" || !emailfilter.test(this.c_email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insertCustomer').find('#c_email').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.c_address.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#c_address').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_gender.value == "" || !genderfilter.test(this.c_gender.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#c_gender').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_contact.value == "" || !contactfilter.test(this.c_contact.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#c_contact').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_password.value == "" || !passwordfilter.test(this.c_password.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#c_password').focus();
                                         return false;
                                         
                                     }
                                     else{
                                     
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/insertCustomer.php",
                                        data:$('#insertCustomer').serialize(),
                                        success: function(data){  
                                        $('#insertCustomer').each(function(){
                                        this.reset();
                                        location.reload(true);
                                        return false;
                                    });
                                } 
                            });
                   return false;   
                  }
                                     
                    });
                  var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#insCustomer').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
               </script>
  <script type="text/javascript" language="javascript">
     /* Ajax call for Customer Display*/
     cus={}
           function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/selectCustomers.php",
                    success: function(data) {
                      
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cus[article.c_id]={};
                            cus[article.c_id]["c_name"]=article.c_name;
                            cus[article.c_id]["c_gender"]=article.c_gender;
                            cus[article.c_id]["c_dob"]=article.c_dob;
                            cus[article.c_id]["c_contact"]=article.c_contact;
                            cus[article.c_id]["c_password"]=article.c_password;
                            cus[article.c_id]["c_email"]=article.c_email;
                            cus[article.c_id]["c_date"]=article.c_date;
                            cus[article.c_id]["c_address"]=article.c_address;
                            
                            $("#data >tbody").append($('<tr/>')
                                    .append($('<td/>').html((index+1)))
                                    .append($('<td/>').html(article.c_name))
                                    .append($('<td/>').html(article.c_gender))
                                    .append($('<td/>').html(article.c_dob))
                                    .append($('<td/>').html(article.c_contact))
                                    .append($('<td/>').html(article.c_password))
                                    .append($('<td/>').html(article.c_email))
                                    .append($('<td/>').html(article.c_date))
                                    .append($('<td/>').html(article.c_address))
                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-c_id="+article.c_id+"  id=\"edit\" data-toggle=\"modal\" data-target=\"#editcustomer\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-c_id="+article.c_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                );
                        });
                        
                    }
                });
         }
               </script>
<script type="text/javascript" language="javascript">
        /* Ajax call for Edit Customer */
         $(document).on("click", "#edit", function () {
                 var c_id = $(this).data('c_id');
                
                 $("#editcustomer").on('shown.bs.modal', function(){
                                    $("#editcustomer").find("#c_id").val(c_id);  
                                    $("#editcustomer").find("#c_name").val(cus[c_id]["c_name"]); 
                                    $("#editcustnmer").find(".select2").val(cus[c_id]["c_gender"]);
                                    $("#editcustomer").find("#c_dob").val(cus[c_id]["c_dob"]);  
                                    $("#editcustomer").find("#c_contact").val(cus[c_id]["c_contact"]);  
                                    $("#editcustomer").find("#c_password").val(cus[c_id]["c_password"]);  
                                    $("#editcustomer").find("#c_email").val(cus[c_id]["c_email"]);  
                                    $("#editcustomer").find("#c_date").val(cus[c_id]["c_date"]);  
                                    $("#editcustomer").find("#c_address").val(cus[c_id]["c_address"]);  
                                    $('#updateCustomer').submit(function() {
                                        
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                      var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     
                                        var passwordfilter = /^\d{4}$/;
                                        if(c_id=="1"){
                                            passwordfilter = /^\d{10}$/;
                                        }
                                     
                                     if(this.c_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#c_name').focus();
                                        return false;
                                       }else if(this.c_dob.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                           $('#updateCustomer').find('#c_dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_email.value == "" || !emailfilter.test(this.c_email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#c_email').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.c_address.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#c_address').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_gender.value == "" || !genderfilter.test(this.c_gender.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#c_gender').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_contact.value == "" || !contactfilter.test(this.c_contact.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                         $('#updateCustomer').find('#c_contact').focus();
                                         return false;
                                         
                                     }
                                     else if(this.c_password.value == "" || !passwordfilter.test(this.c_password.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#c_password').focus();
                                         return false;
                                         
                                     }
                                     else{
                                            $.ajax({
                                            type:"post",
                                            url:"../server/controller/updateCustomer.php",
                                            data:$('#updateCustomer').serialize(),
                                            success: function(data){ 
                                            $('#updateCustomer').each(function(){
                                            this.reset();
                                            location.reload(true);
                                            $('#editcustomer').modal('hide');
                                            return false;
                                            });
                                            } 
                                            });
                                            return false;
                                     }
                              
                      
                   
    });
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#editcustomer').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});
</script>
<script type="text/javascript" language="javascript">
 /* Ajax call for Delete Customer */
    $(document).on("click", "#delete", function () {
                 var c_id = $(this).data('c_id');
                        if(c_id===1){
                            $("#delete1").on('shown.bs.modal', function(){ 
                                $("#delete1").find(".modal-title").empty();   
                                $("#delete1").find(".modal-title").addClass("text-danger").append("Warning");   
                                $("#delete1").find("#msg").empty();   
                                   $("#delete1").find("#msg").append("Sorry, You can not delete admin.");   
                                   $("#delete1").find("#delete2").css({'display':'none'}); 
                                   
                               return false; 
                            });
                        }else{
                     $("#delete1").on('shown.bs.modal', function(){
                          $("#delete1").find(".modal-title").empty();
                          $("#delete1").find(".modal-title").append("Delete Customer");
                          $("#delete1").find("#msg").empty();  
                          $("#delete1").find("#msg").append("Sure to want to delete ?");  
                          $("#delete1").find(".modal-footer").find("#delete2").css({'display':''}); 
                    $("#delete1").find("#c_id").val(c_id);        
                        $('#deleteCustomer').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteCustomer.php",
                            data:"c_id="+c_id,
                            success: function(data){ 
                                
                                $("#a").append(data);
                                if(data.trim()!="Error"){
                                     $('#deleteCustomer').each(function(){
                                this.reset();
                                        onlad()
                                        $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                }else{
                                     $('#confirm').modal('show');
                                }
                             
                                } 
                  });
                    return false;
    });
    
    });
                        }
       
   
});
  /* Code for Customer Delete end */  
 
</script>
  </body>
</html>

