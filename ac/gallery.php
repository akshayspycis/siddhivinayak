<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Gallery</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          image={}
          $(document).ready(function(){
              onlad();
            });  
     </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS iidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  iidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left iide column. contains the iidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Feedbacks</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Gallery</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addImage">Add Photos</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Image</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insImage" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Images </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="sel1" >Select Category</label>
                            <select class="form-control select2" id="icat" name="icat">
                                <option value="0">-Select-</option>
                            </select>
<!--                            <select class="form-control" name="icat" id="icat">
                                    <option value="">NDLM</option>
                                    <option value="">UIDAI</option>
                                    <option value="">OBC & MINORITY</option>
                                    <option value="">PMKVY</option>
                            </select>-->
                            </div>
                        </div><!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                        <label for="image">Image</label>
                        <div class="box-body">
                          <div class="uploadArea" id="dragAndDropFiles" style="min-height: 60px;">
                              <h1 style="font-size: 26px;">Drop Images Here</h1>
                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="ipath" style="width: 0px; height: 0px; overflow: hidden;">
                              
                          </div>
                        </div>
                      <p class="help-block"></p>
                     </div>
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->
        <div class="modal-footer">
        <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>

      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Image </h4>
        </div>
        <form id ="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="iid" id="iid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>
  <script type="text/javascript" language="javascript">
      var obj_class=null;
      $(document).ready(function(){
        $("#addImage").click(function(){
            $("#insImage").modal('show');}); 
                $("#insImage").on('shown.bs.modal', function(){
                  $('#insertform').find('#icat').empty();
                            $.ajax({
                                    type:"post",
                                    url:"../server/controller/selimgCategory.php",
                                    success: function(data) { 
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article) {
                                        var a=$("<option></option>").append(article.name).attr({'value':article.icat});
                                         $('#insertform').find('#icat').append(a);                    
                                    });
                                    }
                                    });  
                    
                var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "insertform", // Form ID
                        dragArea: "dragAndDropFiles", // Upload Area ID
                        multiUpload:"multiUpload",
                        url:"../server/controller/insertImage.php",
                        onlad:onlad,
                        strlenght:25,
                        module:1
                        }
                        if(obj_class==null){
                           obj_class=new $.UploadImg(obj);           
                        }
                                   /* ajax call for  select category   */
   
    });
   }); 
         
  /* Code for Product Edit   start */  
  var upd_img=false;
  // this when we want to update image above var is set value;
$(document).on("click", ".close", function () {
    var iid=$(this).attr('id');
    $(this).next('span').empty();
    var form = $("<form>").attr('id','update_img')
    var a=$("<div>").addClass("uploadArea").attr('id','dragAndDropFiles1').css('min-height','97px')
        .append($("<h1>").css('font-size','16px').append("Drop Images Here"))
        .append($("<input/>").css({'width':'0px','height':'0px','overflow':'hidden'}).attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':'multiUpload1'}))
         var upd_button=$("<button>").attr({'type':'button','class':'btn btn-default btn-sm'}).append($("<i>").addClass("fa fa-upload "))
                 .css({'margin-bottom':'5px'})
         form.append(upd_button)
         form.append(a)
         $(this).next('span').append(form);
         var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "update_img", // Form ID
                        dragArea: "dragAndDropFiles1", // Upload Area ID
                        multiUpload:"multiUpload1",// input field id
                        url:"../server/controller/updImage.php",//location call controller
                        onlad:onlad,
                        iid:iid,
                        upd_button:upd_button,
                        strlenght:25,
                        module:1
                        }
                        new $.UploadImg(obj);           
});


</script>

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var iid = $(this).data('iid');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#iid").val(iid);        
                        $('#deleteimage').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteImage.php",
                            data:"iid="+iid,
                            success: function(data){ 
                               
                        $('#deleteimage').each(function(){
                                this.reset();
                 $.ajax({
                    type:"post",
                    url:"../server/controller/selectImage.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            image[article.iid]={};
                            image[article.iid]["ipath"]=article.ipath;
                            image[article.iid]["icat"]=article.icat;
                            image[article.iid]["idate"]=article.idate;
                            image[article.iid]["itime"]=article.itime;
                           var img= "../server/controller/"+article.ipath;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.icat))
                                .append($('<td/>').html(article.idate))
                                .append($('<td/>').html(article.itime))
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-iid="+article.iid+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editFeedback\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-iid="+article.iid+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................
jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {	
                       
                             var inputId = $("#"+this.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <2){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
//                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
//                                that._deleteFiles($(this).parent().parent().attr('rel'));
//                                $(this).parent().parent().remove();
//                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                
                                if(that.up_obj.upd_button==null){
                                   
                                }
                                
                            }
                            $("#"+this.up_obj.dragArea).empty();
                            
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 1 files.")
                            }
                        }
                     
                        
                this.setStream = function(stream,iid){
                    that.all[iid]["byte_stream"]=stream;
                    $(".dfiles[rel='"+iid+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                   if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>')
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        
                      var image = {};  
                      if(that.up_obj.upd_button==null){
                          $('#insertform').find(":input").each(function() {
                            
                            if($(this).attr("name")=="icat"){
                                image["icat"]= $(this).val();
                            }
                           
                           
                        });
                          }else{
                              image['iid']=that.up_obj.iid;
                          }
                        
                      
                      image["ipath"] =file["byte_stream"] ;
                            $.ajax({
                            type:"post",
                            url:that.up_obj.url,
                            data:image,
                            success: function(data){
                             
                             if(that.up_obj.upd_button!=null){
                                  that.up_obj.upd_button.empty();
                                         that.up_obj.upd_button.append($("<i>").addClass("fa fa-check-square-o"))
                                         that.up_obj.onlad();
                                     }else{
                                       
                                          $("#dragAndDropFiles").find("canvas").remove();
                                          $("#dragAndDropFiles").append('<h1 style="font-size: 26px;">Drop Images Here</h1><input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="ipath" style="width: 0px; height: 0px; overflow: hidden;">');
                                            that._deleteFiles(key);
                                            $(".dfiles[rel='"+ids+"']").remove();
                                            //
                                            $('#insertform').each(function(){
                                            this.reset();
                                            that.up_obj.onlad();
                                            return false;
                                        });
                                     }   
                                    } 
                                });
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
   
                this._startUpload = function(){
                    if(that.up_obj.upd_button==null){
                        
                            var reg = /^\\d+$/;
                            var b=true;
                            $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="icat" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill  Category").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                    
                            });

                            if(b==false){
                            return b;
                            }
                            if(Object.keys(that.all).length==0){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please upload  Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        return false;
                            }else{
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }else{
                            if(Object.keys(that.all).length==1){
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }
                    
                    
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,iid) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,iid),iid)
              }
            };
        }

                this.readfiles=function(files,iid) {
                    that.processfile(files,iid); // process each file at once
                }

                this.resizeMe=function (img,iid) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[iid]["image"]=canvas1;
             
                  var canvas2 = document.createElement('canvas');
              canvas2.width =100;
              canvas2.height = 100;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 100, 100);
                                $("#"+this.up_obj.dragArea).append(canvas2);
                                 if(that.up_obj.upd_button!=null){
                                that.up_obj.upd_button.click(function (){
                                    that.up_obj.upd_button.empty();
                                    that.up_obj.upd_button.append('<img src="../style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>');
                                   that._startUpload()
                                });
                                }
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
function onlad(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/selectImage.php",
                    success: function(data) {
                       
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            image[article.iid]={};
                            image[article.iid]["ipath"]=article.ipath;
                            image[article.iid]["icat"]=article.icat;
                            image[article.iid]["idate"]=article.idate;
                            image[article.iid]["itime"]=article.itime;
                           var img= "../server/controller/"+article.ipath;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.icat))
                                .append($('<td/>').html(article.idate))
                                .append($('<td/>').html(article.itime))
//                              .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-iid="+article.iid+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editFeedback\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-iid="+article.iid+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
                }
            









</script>


</html>


