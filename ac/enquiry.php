<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Enquiry</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
        
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
              onlad("");
            });  
    </script> 
     
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
    <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
 <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Enquiry</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Enquiry</h3>
                  
                
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-reflow table-hover ">
                    <thead>
                      <tr>
                        <th width="5%">S.No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message</th>
                        <!--<th colspan="2">Action</th>-->
                      </tr>
                    </thead>
                    <tbody>
                        
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        
                        <th><button class="btn btn-block btn-sm">Print &nbsp;&nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-print"></span></button></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>         
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
    <?php include 'includes/footer.php';?>
               
      
               <!--Modal For Update Customer Details-->
  
     <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Customer </h4>
        </div>
        <form id ="deleteCustomer">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="c_id" id="c_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
       
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
  <script type="text/javascript" language="javascript">
           function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/selectEnquiry.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            $("#data >tbody").append($('<tr/>')
                                    .append($('<td/>').html("MDSITIWOJ"+article.contact_id))
                                    .append($('<td/>').html(article.name2))
                                    .append($('<td/>').html(article.email2))
                                    .append($('<td/>').html(article.message2))
//                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-c_id="+article.c_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                );
                        });
                        
                    }
                });
         }
               </script>

<script type="text/javascript" language="javascript">
 /* Ajax call for Delete Customer */
    $(document).on("click", "#delete", function () {
                 var c_id = $(this).data('c_id');
                     $("#delete1").on('shown.bs.modal', function(){
                          $("#delete1").find(".modal-title").empty();
                          $("#delete1").find(".modal-title").append("Delete Customer");
                          $("#delete1").find("#msg").empty();  
                          $("#delete1").find("#msg").append("Sure to want to delete ?");  
                          $("#delete1").find(".modal-footer").find("#delete2").css({'display':''}); 
                    $("#delete1").find("#c_id").val(c_id);        
                        $('#deleteCustomer').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteCustomer.php",
                            data:"c_id="+c_id,
                            success: function(data){ 
                                
                                $("#a").append(data);
                                if(data.trim()!="Error"){
                                     $('#deleteCustomer').each(function(){
                                this.reset();
                                        onlad()
                                        $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                }else{
                                     $('#confirm').modal('show');
                                }
                             
                                } 
                  });
                    return false;
    });
    
    });
                        
   
});
  /* Code for Customer Delete end */  
 
</script>
  </body>
</html>

