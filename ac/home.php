<!DOCTYPE html>
<html>
  <head>
      
    <meta charset="UTF-8">
    <title></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
    <style>
 .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background:yellowgreen;
  color:black;
  font-weight:bold;
}
    </style>
           
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
<?php
date_default_timezone_set("Asia/Calcutta");
?>
   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
        <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
         
        <!-- Main content -->
        <section class="content">
                <div class="row">
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                <div class="inner">
                <h3><?php echo date("h:i"); ?></h3>
                <p><?php echo date("d-m-Y"); ?></p>
                </div>
                <div class="icon">
                <i class="glyphicon glyphicon-time"></i>
                </div>
                    <a href="home.php" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                <div class="inner">
                <h3 id ="tcustomer">2</h3>
                <p>News</p>
                </div>
                <div class="icon">
                <i class="ion ion-stats-bars"></i>
                </div>
                    <a href="customer.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                <div class="inner">
                <h3 id ="tproduct">2</h3>
                <p>Queries</p>
                </div>
                <div class="icon">
                <i class="ion ion-person-add"></i>
                </div>
                    <a href="products.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-light-blue">
                <div class="inner">
                <h3 id ="torder">2</h3>
                <p>Gallery</p>
                </div>
                <div class="icon">
                <i class="ion ion-pie-graph"></i>
                </div>
                    <a href="orders.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                </div><!-- /.row -->
                <div class="row">
            <div class="col-sm-12">
           

<!--              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Orders</h3>
                </div> /.box-header 
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th>S. No.</th>
                        <th>Customer Name</th>
                        <th>Order Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Detail</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Ankit Pastor</td>
                        <td>23-10-2015</td>
                        <td>1000</td>
                        <td><span class="label label-warning">Pending</span></td>
                        <th><a href="" class="text-black">View Detail</a></th>
                      </tr>
                        <tr>
                        <td>2</td>
                        <td>Ankit Pastor</td>
                        <td>23-10-2015</td>
                        <td>1000</td>
                        <td><span class="label label-success">Completed</span></td>
                        <th><a href="" class="text-black">View Detail</a></th>
                      </tr>
                        <tr>
                        <td>3</td>
                        <td>Ankit Pastor</td>
                        <td>23-10-2015</td>
                        <td>1000</td>
                        <td><span class="label label-warning">Pending</span></td>
                        <th><a href="" class="text-black">View Detail</a></th>
                      </tr>
                 </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th>Total Orders : 5</th>
                        <th>Total Amount : </th>
                        <th><i class="fa fa-inr"></i>&nbsp;3000</th>
                        <th><a href="" class="text-black">View All Orders</a></th>
                        <th><button class="btn btn-block btn-sm">Print &nbsp;&nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-print"></span></button></th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>         
                
                </div> /.box-body 
              </div>-->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>
    <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
  </body>
</html>
