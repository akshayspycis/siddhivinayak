<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Upload/download</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
    
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS iidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  iidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
            <?php
            $dbhost = "localhost";
                    $dbuser = "infopark_siddhiv";
                    $dbpass = 'WIy$T?Ip)8[(';
                    $dbname = "infopark_siddhivinayak";
                    $conn= mysqli_connect($dbhost,$dbuser,$dbpass) or die('cannot connect to the server'); 
                    mysqli_select_db($conn,$dbname) or die('database selection problem');
            if(isset($_POST['btn-upload']))
            {    
                
            $file = rand(1000,100000)."-".$_FILES['file']['name'];
            $file_loc = $_FILES['file']['tmp_name'];
            $file_size = $_FILES['file']['size'];
            $file_type = $_FILES['file']['type'];
            $folder="uploads/";
                
            // new file size in KB
            $new_size = $file_size/1024;  
            // new file size in KB
                
            // make file name in lower case
            $new_file_name = strtolower($file);
            // make file name in lower case
                
            $final_file=str_replace(' ','-',$new_file_name);
                
            if(move_uploaded_file($file_loc,$folder.$final_file))
            {
            $sql="INSERT INTO tbl_uploads(file,type,size) VALUES('$final_file','$file_type','$new_size')";
            mysqli_query($conn,$sql);
            ?>
        <script>
            alert('successfully uploaded');
            window.location.href='downloadsection.php?success';
        </script>
            <?php
            }
            else
            {
            ?>
        <script>
            alert('error while uploading file');
            window.location.href='downloadsection.php?fail';
        </script>
            <?php
            }
            }
            ?>
   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left iide column. contains the iidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Downloads</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Upload Files</h3>
                  
                </div><!-- /.box-header -->
                
                <div class="box-body">
                   
                          
                    <form action="downloadsection.php" method="post" enctype="multipart/form-data">
                    <div class="col-md-3"> 
                     
                          <input type="file" name="file" />
                     </div>
                   <div class="col-md-4">
                       <button type="submit" name="btn-upload">upload</button>
                    
                   
                  </div>         
                </form>
                    <p>&nbsp;</p>   
                  
                    <?php
                    if(isset($_GET['success']))
                    {
                    ?>
                      <label>File Uploaded Successfully... <a href="view.php">click here to view file.</a></label>
                    <?php
                    }
                    else if(isset($_GET['fail']))
                    {
                    ?>
                      <label>Problem While File Uploading !</label>
                    <?php
                    }
                    else
                    {
                    ?>
                      <label>Try to upload any files(PDF, DOC, EXE, VIDEO, MP3, ZIP,etc...)</label>
                    <?php
                    }
                    ?>
                      <p>&nbsp;</p>  
                      
                      <h1 class="box-title text-center text-success">Your Uploaded Files</h1>
                    
                      <p>&nbsp;</p> 
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        
                        <th>File Name</th>
                        <th>File Type</th>
                        <th>File Size(KB)</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                        <tbody>
                        <?php
                        $sql="SELECT * FROM tbl_uploads";
                        $result_set=mysqli_query($conn,$sql);
                        while($row=mysqli_fetch_array($result_set))
                        {
                        ?>
                        <tr>
                        <td><?php echo $row['file'] ?></td>
                        <td><?php echo $row['type'] ?></td>
                        <td><?php echo $row['size'] ?></td>
                        <td><a class="btn btn-success" href="uploads/<?php echo $row['file'] ?>" target="_blank">view file</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>      
              
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
   

    
  </body>



</html>


