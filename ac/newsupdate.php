<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>News</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          news={}
          $(document).ready(function(){
              onlad();
          });  
      </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-green  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">News & Updates</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">News & Updates</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addNews">Add News</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Heading</th>
                        <th>Content</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th>posted On</th>
                        
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insNews" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert News </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="student name">News Heading</label>
                            <input type="text" class="form-control" name="nheading" placeholder="Heading here">
                        </div>
                  <!-- /.form-group -->
                 
                        <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control"  name="ndate">
                        </div>
                        <div class="form-group">
                        <label for="image">Add Image</label>
                        <div class="box-body">
                          <div class="uploadArea" id="dragAndDropFiles" style="min-height: 60px;">
                              <h1 style="font-size: 26px;">Drop Images Here</h1>
                              
                                  <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                              
                          </div>
                        </div>
                      <p class="help-block"></p>
                     </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                 <div class="form-group" id="category">
                    <label>News Content</label>
                    <textarea class="form-control" rows="5" id="message"  name="ncontent" style="resize:none" placeholder="Content here"></textarea>
                  </div><!-- /.form-group -->
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
   <div class="modal fade" id="editNews" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit news </h4>
        </div>
        <form id ="updateform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="news heading">Name</label>
                            <input type="hidden" class="form-control" name="nid" id="nid" value="">
                            <input type="text" class="form-control" name="nheading" id="nheading" placeholder="Heading here">
                        </div>
                  <!-- /.form-group -->
                 
                        <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control"  name="ndate" id="ndate">
                        </div>
                        <div class="form-group">
                         <div class="thumbnail col-md-12">
                                <a class="close" id="close_image" data-toggle="tooltip" title="Remove Image" href="#">×</a>
                                <span  id= "imgg" ></span>
                            </div>
                        </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                 <div class="form-group">
                    <label>News Content</label>
                    <textarea class="form-control" rows="5" id="ncontent"  name="ncontent" style="resize:none" placeholder="Content here"></textarea>
                  </div><!-- /.form-group -->
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete News</h4>
        </div>
        <form id ="deleteNews">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="nid" id="nid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>
  <script type="text/javascript" language="javascript">
      var obj_class=null;
      $(document).ready(function(){
        $("#addNews").click(function(){
            $("#insNews").modal('show');}); 
                $("#insNews").on('shown.bs.modal', function(){
                var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "insertform", // Form ID
                        dragArea: "dragAndDropFiles", // Upload Area ID
                        multiUpload:"multiUpload",
                        url:"../server/controller/insertNews.php",
                        onlad:onlad,
                        strlenght:25,
                        module:1
                        }
                        if(obj_class==null){
                           obj_class=new $.UploadImg(obj);           
                        }
                                   /* ajax call for  select category   */
   
    });
   }); 
         
  /* Code for Product Edit   start */  
  var upd_img=false;
  // this when we want to update image above var is set value;
$(document).on("click", ".close", function () {
    var nid=$(this).attr('id');
    $(this).next('span').empty();
    var form = $("<form>").attr('id','update_img')
    var a=$("<div>").addClass("uploadArea").attr('id','dragAndDropFiles1').css('min-height','97px')
        .append($("<h1>").css('font-size','16px').append("Drop Images Here"))
        .append($("<input/>").css({'width':'0px','height':'0px','overflow':'hidden'}).attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':'multiUpload1'}))
         var upd_button=$("<button>").attr({'type':'button','class':'btn btn-default btn-sm'}).append($("<i>").addClass("fa fa-upload "))
                 .css({'margin-bottom':'5px'})
         form.append(upd_button)
         form.append(a)
         $(this).next('span').append(form);
         var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "update_img", // Form ID
                        dragArea: "dragAndDropFiles1", // Upload Area ID
                        multiUpload:"multiUpload1",// input field id
                        url:"../server/controller/updNewsImg.php",//location call controller
                        onlad:onlad,
                        nid:nid,
                        upd_button:upd_button,
                        strlenght:25,
                        module:1
                        }
                      new $.UploadImg(obj);           
});

  $(document).on("click", "#edit", function () {
                 var nid = $(this).data('nid');
                 
                 $("#editNews").on('shown.bs.modal', function(){
                                    $("#editNews").find("#nid").val(nid);  
                                    $("#editNews").find("#nheading").val(news[nid]["nheading"]);  
                                    $("#editNews").find("#ndate").val(news[nid]["ndate"]);  
                                    $("#editNews").find("#nimage").val(news[nid]["nimage"]);  
                                    $("#editNews").find("#ncontent").val(news[nid]["ncontent"]);  
                                     var img= "../server/controller/"+news[nid]["nimage"];
                                     $("#editNews").find("#close_image").attr({'id':nid});
                                     $("#editNews").find("#imgg").empty();
                                    $("#editNews").find("#imgg").append("<img src="+img+" height=\"100\" width=\"100\"/>");  
                                    $('#updateform').submit(function() {
                        $.ajax({
                        type:"post",
                        url:"../server/controller/updateNews.php",
                        data:$('#updateform').serialize(),
                        success: function(data){
                            onlad();
                          $('#updateform').each(function(){
                            this.reset();
                            $('#editNews').modal('hide');
                            return false;
                                    });
                              } 
                            });
                   
                   return false;
    });
   
    });
});
  /* Code for Product Edit   start */  
</script>

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var nid = $(this).data('nid');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#nid").val(nid);        
                        $('#deleteNews').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteNews.php",
                            data:"nid="+nid,
                            success: function(data){ 
                              
                        $('#deleteNews').each(function(){
                                this.reset();
                 $.ajax({
                    type:"post",
                    url:"../server/controller/selectNews.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            news[article.nid]={};
                            news[article.nid]["nheading"]=article.nheading;
                            news[article.nid]["ncontent"]=article.ncontent;
                            news[article.nid]["ndate"]=article.ndate;
                            news[article.nid]["nposted"]=article.nposted;
                            news[article.nid]["nimage"]=article.nimage;
                           var img= "../server/controller/"+article.nimage;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.nheading))
                                .append($('<td/>').html(article.ncontent))
                                .append($('<td/>').html(article.ndate))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.nposted))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-nid="+article.nid+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editNews\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-nid="+article.nid+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................
jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {	
                       
                             var inputId = $("#"+this.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <2){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
//                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
//                                that._deleteFiles($(this).parent().parent().attr('rel'));
//                                $(this).parent().parent().remove();
//                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                
                                if(that.up_obj.upd_button==null){
                                   
                                }
                                
                            }
                            $("#"+this.up_obj.dragArea).empty();
                            
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 1 files.")
                            }
                        }
                     
                        
                this.setStream = function(stream,nid){
                    that.all[nid]["byte_stream"]=stream;
                    $(".dfiles[rel='"+nid+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                   if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>')
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        
                      var news = {};  
                      if(that.up_obj.upd_button==null){
                          $('#insertform').find(":input").each(function() {
                            
                            if($(this).attr("name")=="nheading"){
                                news["nheading"]= $(this).val();
                            }
                            
                            if($(this).attr("name")=="ndate"){
                                news["ndate"]= $(this).val();
                            }
                            if($(this).attr("name")=="ncontent"){
                                news["ncontent"]= $(this).val();
                            }
                           
                        });
                          }else{
                              news['nid']=that.up_obj.nid;
                          }
                        
                      
                      news["nimage"] =file["byte_stream"] ;
                            $.ajax({
                            type:"post",
                            url:that.up_obj.url,
                            data:news,
                            success: function(data){
                             if(that.up_obj.upd_button!=null){
                                  that.up_obj.upd_button.empty();
                                         that.up_obj.upd_button.append($("<i>").addClass("fa fa-check-square-o"))
                                         that.up_obj.onlad();
                                     }else{
                                       
                                          $("#dragAndDropFiles").find("canvas").remove();
                                          $("#dragAndDropFiles").append('<h1 style="font-size: 26px;">Drop Images Here</h1><input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">');
                                            that._deleteFiles(key);
                                            $(".dfiles[rel='"+ids+"']").remove();
                                            //
                                            $('#insertform').each(function(){
                                            this.reset();
                                            that.up_obj.onlad();
                                            return false;
                                        });
                                     }   
                                    } 
                                });
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
   
                this._startUpload = function(){
                    if(that.up_obj.upd_button==null){
                        
                            var reg = /^\\d+$/;
                            var b=true;
                            $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="nheading" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill  Heading").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                     if($(this).attr("name")=="ndate" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                    if($(this).attr("name")=="ncontent" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill News Detail").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                            });

                            if(b==false){
                            return b;
                            }
                            if(Object.keys(that.all).length==0){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please upload  Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        return false;
                            }else{
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }else{
                            if(Object.keys(that.all).length==1){
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }
                    
                    
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,nid) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,nid),nid)
              }
            };
        }

                this.readfiles=function(files,nid) {
                    that.processfile(files,nid); // process each file at once
                }

                this.resizeMe=function (img,nid) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[nid]["image"]=canvas1;
             
                  var canvas2 = document.createElement('canvas');
              canvas2.width =100;
              canvas2.height = 100;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 100, 100);
                                $("#"+this.up_obj.dragArea).append(canvas2);
                                 if(that.up_obj.upd_button!=null){
                                that.up_obj.upd_button.click(function (){
                                    that.up_obj.upd_button.empty();
                                    that.up_obj.upd_button.append('<img src="../style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>');
                                   that._startUpload()
                                });
                                }
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
function onlad(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/selectNews.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            news[article.nid]={};
                            news[article.nid]["nheading"]=article.nheading;
                            news[article.nid]["ncontent"]=article.ncontent;
                            news[article.nid]["ndate"]=article.ndate;
                            news[article.nid]["nposted"]=article.nposted;
                            news[article.nid]["nimage"]=article.nimage;
                           var img= "../server/controller/"+article.nimage;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.nheading))
                                .append($('<td/>').html(article.ncontent))
                                .append($('<td/>').html(article.ndate))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.nposted))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-nid="+article.nid+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editNews\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-nid="+article.nid+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
                }
            
</script>


</html>

